﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SKGL;

namespace license_script
{
    enum ExitCode : int
    {
        Failure = 0,
        Success = 1,
        Error = 2
    }

    class Program
    {
        static int Main(string[] args)
        {
            int exitCode = (int)ExitCode.Error;
            if (args.Length > 0)
            {
                while ((int)exitCode == (int)ExitCode.Error)
                {
                    for (int i = 0; i < args.Length; i++)
                    {
                        //Console.WriteLine(args[i]);
                        if (args[0] == "check")
                        {
                            string key = args[1];
                            string id = args[2];

                            bool validity = false;

                            SKGL.Validate val = new Validate();
                            val.secretPhase = id;
                            val.Key = key;

                            validity = val.IsValid;

                            if (validity == true)
                            {
                                exitCode = (int)ExitCode.Success;
                                Console.WriteLine("valid");
                            }
                            else
                            {
                                exitCode = (int)ExitCode.Failure;
                                Console.WriteLine("invalid");
                            }

                            val = null;
                        }
                        else if (args[0] == "create")
                        {
                            string id = args[1];

                            SKGL.Generate key = new SKGL.Generate();
                            key.secretPhase = id;
                            key.doKey(90);

                            Console.WriteLine(key.Key);
                            exitCode = (int)ExitCode.Success;
                        }
                        else
                        {
                            exitCode = (int)ExitCode.Failure;
                        }
                    }
                }
            }

            return exitCode;
        }
    }
}
