﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discoverse.Systems;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using BreakerUI;
using BreakerUI.Components;
using System.Reflection;
using System.IO;
using System.Collections;
using System.Diagnostics;

namespace Discoverse.Screens
{
    public class LightingScreen : GameScreen
    {
        SpriteFont font;

        Manager manager;

        #region Lighting Variables
        Texture2D texture;
        Texture2D normal;
        List<RenderTarget2D> renderTargets;

        Effect lightEffect;
        Effect combinedLightEffect;
        private EffectTechnique _lightEffectTechniquePointLight;
        private EffectTechnique _lightEffectTechniqueSpotLight;
        private EffectParameter _lightEffectParameterStrength;
        private EffectParameter _lightEffectParameterPosition;
        private EffectParameter _lightEffectParameterLightColor;
        private EffectParameter _lightEffectParameterLightDecay;
        private EffectParameter _lightEffectParameterScreenWidth;
        private EffectParameter _lightEffectParameterScreenHeight;
        private EffectParameter _lightEffectParameterNormapMap;

        private EffectParameter _lightEffectParameterConeAngle;
        private EffectParameter _lightEffectParameterConeDecay;
        private EffectParameter _lightEffectParameterConeDirection;

        private EffectTechnique _lightCombinedEffectTechnique;
        private EffectParameter _lightCombinedEffectParamAmbient;
        private EffectParameter _lightCombinedEffectParamLightAmbient;
        private EffectParameter _lightCombinedEffectParamAmbientColor;
        private EffectParameter _lightCombinedEffectParamColorMap;
        private EffectParameter _lightCombinedEffectParamShadowMap;
        private EffectParameter _lightCombinedEffectParamNormalMap;

        VertexPositionColorTexture[] vertices;
        VertexBuffer vertexBuffer;

        private List<Light> lights = new List<Light>();
        #endregion
        private Color _ambientLight = new Color(0.1f, 0.1f, 0.1f, 1);
        private float _specularStrength = .5f;

        int dayLength;
        int dayStart;
        int nightStart;
        int currentTime;
        float cycleSpeed;

        bool isDay;
        Vector3 sunPos;
        pointLight sun;
        Coroutine c = new Coroutine();

        public override void Init()
        {

            base.Init();
        }

        public override void LoadContent()
        {
            backgroundColor = Color.FromNonPremultiplied(0, 102, 153, 255);
            font = ScreenManager.Content.getFont("Express");

            renderTargets = new List<RenderTarget2D>();

            PresentationParameters pp = ScreenManager.SpriteBatch.GraphicsDevice.PresentationParameters;
            int width = pp.BackBufferWidth;
            int height = pp.BackBufferHeight;
            SurfaceFormat format = pp.BackBufferFormat;

            renderTargets.Add(new RenderTarget2D(ScreenManager.SpriteBatch.GraphicsDevice, width, height)); // Color
            renderTargets.Add(new RenderTarget2D(ScreenManager.SpriteBatch.GraphicsDevice, width, height)); // Normal
            SurfaceFormat sformat = ScreenManager.SpriteBatch.GraphicsDevice.PresentationParameters.BackBufferFormat;
            DepthFormat dformat = ScreenManager.SpriteBatch.GraphicsDevice.PresentationParameters.DepthStencilFormat;
            int msamplec = ScreenManager.SpriteBatch.GraphicsDevice.PresentationParameters.MultiSampleCount;
            renderTargets.Add(new RenderTarget2D(ScreenManager.SpriteBatch.GraphicsDevice, width, height, false, sformat, dformat, msamplec, RenderTargetUsage.DiscardContents)); // Shadow

            lightEffect = ScreenManager.Content.getEffect("MultiTarget");
            combinedLightEffect = ScreenManager.Content.getEffect("DeferredCombined");

            texture = ScreenManager.Content.addTexture("fxdecal_glass", "Textures/fxdecal_glass");
            normal = ScreenManager.Content.addTexture("fxdecal_glass_n", "Textures/fxdecal_glass_n");
            /*texture = ScreenManager.Content.Load<Texture2D>("Textures/face");
            normal = ScreenManager.Content.Load<Texture2D>("Textures/face_normals");*/

            vertices = new VertexPositionColorTexture[4];
            vertices[0] = new VertexPositionColorTexture(new Vector3(-1, 1, 0), Color.White, new Vector2(0, 0));
            vertices[1] = new VertexPositionColorTexture(new Vector3(1, 1, 0), Color.White, new Vector2(1, 0));
            vertices[2] = new VertexPositionColorTexture(new Vector3(-1, -1, 0), Color.White, new Vector2(0, 1));
            vertices[3] = new VertexPositionColorTexture(new Vector3(1, -1, 0), Color.White, new Vector2(1, 1));
            vertexBuffer = new VertexBuffer(ScreenManager.SpriteBatch.GraphicsDevice, typeof(VertexPositionColorTexture), vertices.Length, BufferUsage.None);
            vertexBuffer.SetData(vertices);

            // Point light technique
            _lightEffectTechniquePointLight = lightEffect.Techniques["DeferredPointLight"];
            // Spot light technique
            _lightEffectTechniqueSpotLight = lightEffect.Techniques["DeferredSpotLight"];
            // Shared light properties
            _lightEffectParameterLightColor = lightEffect.Parameters["lightColor"];
            _lightEffectParameterLightDecay = lightEffect.Parameters["lightDecay"];
            _lightEffectParameterNormapMap = lightEffect.Parameters["NormalMap"];
            _lightEffectParameterPosition = lightEffect.Parameters["lightPosition"];
            _lightEffectParameterScreenHeight = lightEffect.Parameters["screenHeight"];
            _lightEffectParameterScreenWidth = lightEffect.Parameters["screenWidth"];
            _lightEffectParameterStrength = lightEffect.Parameters["lightStrength"];
            // Spot light parameters
            _lightEffectParameterConeDirection = lightEffect.Parameters["coneDirection"];
            _lightEffectParameterConeAngle = lightEffect.Parameters["coneAngle"];
            _lightEffectParameterConeDecay = lightEffect.Parameters["coneDecay"];
            _lightCombinedEffectTechnique = combinedLightEffect.Techniques["DeferredCombined2"];
            _lightCombinedEffectParamAmbient = combinedLightEffect.Parameters["ambient"];
            _lightCombinedEffectParamLightAmbient = combinedLightEffect.Parameters["lightAmbient"];
            _lightCombinedEffectParamAmbientColor = combinedLightEffect.Parameters["ambientColor"];
            _lightCombinedEffectParamColorMap = combinedLightEffect.Parameters["ColorMap"];
            _lightCombinedEffectParamShadowMap = combinedLightEffect.Parameters["ShadingMap"];
            _lightCombinedEffectParamNormalMap = combinedLightEffect.Parameters["NormalMap"];

            Random rnd = new Random();
            lights.Add(new spotLight()
            {
                isEnabled = false,
                color = new Vector4(1f, 1f, 0f, 1f),
                power = 1f,
                lightDecay = 500,
                position = new Vector3(128, 128, 20),
                spotAngle = 3,
                spotDecayExponent = 9,
                //direction = new Vector3(0.9f, 0.9f, 0),
                SpotRotation = 45f,
            });
            lights.Add(new spotLight()
            {
                isEnabled = false,
                color = new Vector4(1f, 0f, 0f, 1f),
                power = 1f,
                lightDecay = 500,
                position = new Vector3(128, 128, 20),
                spotAngle = 3,
                spotDecayExponent = 9,
                //direction = new Vector3(0.9f, 0.9f, 0),
                SpotRotation = 180f,
            });
            lights.Add(new pointLight()
            {
                isEnabled = true,
                color = new Vector4(1f, 0f, 0f, 1f),
                power = 0.02f,
                lightDecay = 500,
                position = new Vector3(256, 256, 20),
            });

            dayLength = 1440;
            dayStart = 300;
            nightStart = 1200;
            currentTime = 300;

            Button b = new Button(new Vector2(300, 300), "Unload Scripts", ScreenManager.ui_manager);
            b.onButtonClick += B_onButtonClick;
            c.Start(TimeOfDay());
            //manager = new Manager(ScreenManager.Content, ScreenManager.Content.Load<Texture2D>("Textures/GUI"), ScreenManager.GraphicsDevice);

            /*string loc = Assembly.GetExecutingAssembly().Location.Replace("\\Discoverse.exe", "");
            if (File.Exists(loc + "//test.disc"))
            {
                int count = 0;
                string line = "";

                // Read the file and display it line by line.
                System.IO.StreamReader file =
                   new System.IO.StreamReader(loc + "//test.disc");
                while ((line = file.ReadLine()) != null)
                {
                    string[] parts = line.Split(' ');
                    string function = parts[0];
                    if (function == "new")
                    {
                        string item = parts[1];

                        if (item == "Button")
                            new Button(new Vector2(Convert.ToInt32(parts[2]), Convert.ToInt32(parts[3])), parts[4], manager);

                        if (item == "Textbox")
                            new Textbox(new Vector2(Convert.ToInt32(parts[2]), Convert.ToInt32(parts[3])), parts[4], manager);
                            //new Button(new Vector2(Convert.ToInt32(parts[2]), Convert.ToInt32(parts[3])), parts[4], manager);
                    }
                }

                file.Close();
            }*/

            base.LoadContent();
        }

        private void B_onButtonClick(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
            ScreenManager.moddingSandbox.stopAllScripts();
        }

        public override void UnloadContent()
        {
            base.UnloadContent();
        }

        float rot = 0f;

        IEnumerator TimeOfDay()
        {
            while (true)
            {
                currentTime += 1;

                int hours = Convert.ToInt32(currentTime / 60);
                int mins = currentTime % 60;
                yield return Pause(0.1f);
            }
        }

        static IEnumerator Pause(float time)
        {
            var watch = Stopwatch.StartNew();
            while (watch.Elapsed.TotalSeconds < time)
                yield return 0;
        }

        public override void Update(GameTime gameTime)
        {
            rot -= 0.05f;

            lights[0].direction = new Vector3((float)Math.Cos(rot + 45), (float)Math.Sin(rot + 45), currentTime / dayLength);
            lights[1].direction = new Vector3((float)Math.Cos(rot - 90), (float)Math.Sin(rot - 90), lights[0].direction.Z);

            if (c.Running)
                c.Update();

            if (currentTime > 0 && currentTime < dayStart)
            {
                isDay = false;
                if (lights[2].power > 0.02f)
                    lights[2].power -= 0.001f;
            }
            else if (currentTime >= dayStart && currentTime < nightStart)
            {
                isDay = true;
                if (lights[2].power < 0.5f)
                    lights[2].power += 0.00025f;
            }
            else if (currentTime >= nightStart && currentTime < dayLength)
            {
                isDay = false;
                if (lights[2].power > 0.02f)
                    lights[2].power -= 0.001f;
            }
            else if (currentTime >= dayLength)
            {
                currentTime = 0;
                lights[2].power = 0.02f;
            }
            float currentTimeF = currentTime;
            float dayLengthF = dayLength;

            /*lights[0].position = new Vector3(
                ((float)Math.Sin(gameTime.TotalGameTime.TotalSeconds * 2) * 200) + 450,
                ((float)Math.Cos(gameTime.TotalGameTime.TotalSeconds * 2) * 200) + 350,
                lights[0].position.Z);
            lights[1].position = new Vector3(((float)Math.Sin(gameTime.TotalGameTime.TotalSeconds) * 300) + 125, 50, lights[1].position.Z);
            lights[0].position = new Vector3(Mouse.GetState().Position.ToVector2(), lights[0].position.Z);*/

            //manager.Update();
            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.GraphicsDevice.SetRenderTarget(renderTargets[0]);
            spriteBatch.GraphicsDevice.Clear(Color.Transparent);
            DrawColorMap();

            spriteBatch.GraphicsDevice.SetRenderTarget(null);

            spriteBatch.GraphicsDevice.SetRenderTarget(renderTargets[1]);
            spriteBatch.GraphicsDevice.Clear(Color.Transparent);
            DrawNormalMap();
            spriteBatch.GraphicsDevice.SetRenderTarget(null);

            GenerateShadowMap();
            spriteBatch.GraphicsDevice.Clear(Color.Black);
            DrawCombinedMaps();

            //DrawDebugRenderTargets(spriteBatch);

            spriteBatch.Begin();
            //manager.Draw(spriteBatch);
            string hour;
            string min;

            if (currentTime / 60 < 10)
                hour = "0" + (currentTime / 60);
            else
                hour = (currentTime / 60).ToString();

            if (currentTime % 60 < 10)
                min = "0" + (currentTime % 60);
            else
                min = (currentTime % 60).ToString();

            spriteBatch.DrawString(font, "Time: " + hour + ":" + min + "(" + currentTime + ")" + "\nLight Power: " + lights[2].power, Vector2.Zero, Color.White);
            spriteBatch.End();

            base.Draw(spriteBatch);
        }

        #region Lighting Methods
        private void DrawCombinedMaps()
        {
            combinedLightEffect.CurrentTechnique = _lightCombinedEffectTechnique;
            _lightCombinedEffectParamAmbient.SetValue(1f);
            _lightCombinedEffectParamLightAmbient.SetValue(4f);
            _lightCombinedEffectParamAmbientColor.SetValue(_ambientLight.ToVector4());
            _lightCombinedEffectParamColorMap.SetValue(renderTargets[0]);
            _lightCombinedEffectParamShadowMap.SetValue(renderTargets[2]);
            _lightCombinedEffectParamNormalMap.SetValue(renderTargets[1]);
            combinedLightEffect.CurrentTechnique.Passes[0].Apply();

            ScreenManager.SpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, null, null, combinedLightEffect);
            ScreenManager.SpriteBatch.Draw(renderTargets[0], Vector2.Zero, Color.White);
            ScreenManager.SpriteBatch.End();
        }

        /// <summary>
        /// Draws the normal map.
        /// </summary>
        private void DrawNormalMap()
        {
            ScreenManager.SpriteBatch.Begin();
            //ScreenManager.SpriteBatch.Draw(normal, Vector2.Zero, Color.White);
            ScreenManager.SpriteBatch.Draw(normal, new Rectangle(0, 0, 512, 512), new Rectangle(512 * 0, 512 * 1, 512, 512), Color.White);
            ScreenManager.SpriteBatch.End();
        }

        /// <summary>
        /// Draws the color map.
        /// </summary>
        private void DrawColorMap()
        {
            ScreenManager.SpriteBatch.Begin();

            //ScreenManager.SpriteBatch.Draw(texture, Vector2.Zero, Color.White);
            ScreenManager.SpriteBatch.Draw(texture, new Rectangle(0, 0, 512, 512), new Rectangle(512 * 0, 512 * 1, 512, 512), Color.White);
            ScreenManager.SpriteBatch.End();
        }

        /// <summary>
        /// Generates the light map from the ColorMap and NormalMap textures combined with all the active lights.
        /// </summary>
        /// <returns></returns>
        private Texture2D GenerateShadowMap()
        {
            ScreenManager.SpriteBatch.GraphicsDevice.SetRenderTarget(renderTargets[2]);
            ScreenManager.SpriteBatch.GraphicsDevice.Clear(Color.Transparent);

            foreach (Light light in lights)
            {
                if (!light.isEnabled) continue;

                ScreenManager.SpriteBatch.GraphicsDevice.SetVertexBuffer(vertexBuffer);

                // Draw all the light sources
                _lightEffectParameterStrength.SetValue(light.actualPower);
                _lightEffectParameterPosition.SetValue(light.position);
                _lightEffectParameterLightColor.SetValue(light.color);
                _lightEffectParameterLightDecay.SetValue(light.lightDecay); // Value between 0.00 and 2.00
                lightEffect.Parameters["specularStrength"].SetValue(_specularStrength);

                if (light.lightType == LightType.Point)
                {
                    lightEffect.CurrentTechnique = _lightEffectTechniquePointLight;
                }
                else
                {
                    lightEffect.CurrentTechnique = _lightEffectTechniqueSpotLight;
                    _lightEffectParameterConeAngle.SetValue(((spotLight)light).spotAngle);
                    _lightEffectParameterConeDecay.SetValue(((spotLight)light).spotDecayExponent);
                    _lightEffectParameterConeDirection.SetValue(((spotLight)light).direction);
                }

                _lightEffectParameterScreenWidth.SetValue((float)ScreenManager.SpriteBatch.GraphicsDevice.Viewport.Width);
                _lightEffectParameterScreenHeight.SetValue((float)ScreenManager.SpriteBatch.GraphicsDevice.Viewport.Height);
                lightEffect.Parameters["ambientColor"].SetValue(_ambientLight.ToVector4());
                _lightEffectParameterNormapMap.SetValue(renderTargets[1]);
                lightEffect.Parameters["ColorMap"].SetValue(renderTargets[0]);
                lightEffect.CurrentTechnique.Passes[0].Apply();

                // Add Belding (Black background)
                ScreenManager.SpriteBatch.GraphicsDevice.BlendState = BlendBlack;

                // Draw some magic
                //ScreenManager.Instance.device.DrawUserPrimitives(PrimitiveType.TriangleStrip, vertices, 0, 2);
                ScreenManager.SpriteBatch.GraphicsDevice.DrawUserPrimitives(PrimitiveType.TriangleStrip, vertices, 0, 2);
            }

            // Deactive the rander targets to resolve them
            ScreenManager.SpriteBatch.GraphicsDevice.SetRenderTarget(null);

            return renderTargets[2];
        }

        public void DrawDebugRenderTargets(SpriteBatch spriteBatch)
        {
            // Draw some debug textures
            spriteBatch.Begin();

            Rectangle size = new Rectangle(0, 0, renderTargets[0].Width / 3, renderTargets[0].Height / 3);
            var position = new Vector2(0, spriteBatch.GraphicsDevice.Viewport.Height - size.Height);
            /*spriteBatch.Draw(
                renderTargets[0],
                new Rectangle(
                    (int)position.X, (int)position.Y,
                    size.Width,
                    size.Height),
                Color.White);

            spriteBatch.Draw(
                renderTargets[1],
                new Rectangle(
                    (int)position.X + size.Width, (int)position.Y,
                    size.Width,
                    size.Height),
                Color.White);*/

            spriteBatch.Draw(
                renderTargets[2],
                new Rectangle(
                    (int)position.X + size.Width * 2, (int)position.Y,
                    size.Width,
                    size.Height),
                Color.White);

            spriteBatch.End();
        }

        public static BlendState BlendBlack = new BlendState()
        {
            ColorBlendFunction = BlendFunction.Add,
            ColorSourceBlend = Blend.One,
            ColorDestinationBlend = Blend.One,

            AlphaBlendFunction = BlendFunction.Add,
            AlphaSourceBlend = Blend.SourceAlpha,
            AlphaDestinationBlend = Blend.One
        };
        #endregion
    }
}
