﻿using Discoverse.Systems;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FarseerPhysics;
using FarseerPhysics.Collision;
using FarseerPhysics.Collision.Shapes;
using FarseerPhysics.Common;
using FarseerPhysics.Common.ConvexHull;
using FarseerPhysics.Common.Decomposition;
using FarseerPhysics.Common.PhysicsLogic;
using FarseerPhysics.Common.PolygonManipulation;
using FarseerPhysics.Common.TextureTools;
using FarseerPhysics.Controllers;
using FarseerPhysics.DebugView;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using FarseerPhysics.Dynamics.Joints;
using FarseerPhysics.Factories;
using System.Xml.Serialization;

namespace Discoverse.Screens
{
    public class FarseerLevel : GameScreen
    {
        SpriteFont font;

        /* START: Editor Mode Stuff
         * 
         * Default World Speed: 30f (or 60f)
         * Paused World Speed: 2000f
         * 
        */
        public static bool suspended = false;
        public static float worldSpeed = 60f;

        float maxSpeed = 2000f;

        private bool editing = false;

        public bool EditMode
        {
            get { return this.editing; }
        }

        // END: Editor Mode Stuff

        public static Dictionary<Body, PhysicsGameObject> physicsObjects;

        private DebugViewXNA debugView;
        public static World world;

        private float timeStep;

        public float TimeStep
        {
            get { return this.timeStep; }
        }

        private Matrix SimProj;
        private Matrix SimView;

        Vector2 worldPosition;

        Body floor;

        public virtual void loadWorldElements()
        {
        }

        public virtual void unloadWorldElements()
        {
            Content.ContentManager().Unload();
        }

        public virtual void updateWorld(GameTime gameTime)
        {
        }

        public virtual void drawWorld(SpriteBatch spriteBatch)
        {
        }

        public override void LoadContent()
        {
            backgroundColor = Color.CornflowerBlue;
            font = Content.getFont("Express");

            // Setup SimProj
            SimProj = Matrix.CreateOrthographicOffCenter(0f, ConvertUnits.ToSimUnits(Dimensions.X), ConvertUnits.ToSimUnits(Dimensions.Y), 0f, 0f, 10f);
            SimView = Matrix.Identity;

            // Setup world
            if (world == null)
            {
                world = new World(Vector2.UnitY * 9.802f);
            }
            else
            {
                world.Clear();
            }

            // Setup Debug View
            if (debugView == null)
            {
                debugView = new DebugViewXNA(world);
                debugView.AppendFlags(DebugViewFlags.Shape);
                /*debugView.RemoveFlags(DebugViewFlags.Controllers);
                debugView.RemoveFlags(DebugViewFlags.Joint);*/
                debugView.LoadContent(device, Content.ContentManager());
                debugView.Enabled = true;
            }

            // Create floor
            Vector2 floorDim = new Vector2(50, 50);
            floor = BodyFactory.CreateRectangle(world, floorDim.X, ConvertUnits.ToSimUnits(floorDim.Y), 10.0f);
            floor.Position = ConvertUnits.ToSimUnits(floorDim.X / 2, Dimensions.Y - (floorDim.Y / 2));
            floor.IsStatic = true;
            floor.Restitution = 0.2f;
            floor.Friction = 0.2f;
            floor.UserData = "perm";

            loadWorldElements();

            base.LoadContent();
        }

        public override void UnloadContent()
        {
            base.UnloadContent();
        }

        private void toggleSimulation()
        {
            float speed = 60f;
            if (suspended)
            {
                if (worldSpeed <= maxSpeed)
                    worldSpeed += 60f;

                /*if (world.Enabled && worldSpeed >= maxSpeed)
                    world.Enabled = false;*/
            }
            else
            {
                if (worldSpeed > speed)
                    worldSpeed -= speed;

                if (!world.Enabled)
                    world.Enabled = true;
            }
        }

        public static void togglePauseMode()
        {
            if (suspended)
                suspended = false;
            else
                suspended = true;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            toggleSimulation();

            timeStep = Math.Min((float)gameTime.ElapsedGameTime.TotalMilliseconds * 0.001f, (1f / worldSpeed));
            world.Step(timeStep);

            worldPosition = ConvertScreenToWorld(mousePosition);

            updateWorld(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            ScreenManager.GraphicsDevice.Clear(this.backgroundColor);
            spriteBatch.Begin();

            var projection = Matrix.CreateOrthographicOffCenter(0f, ConvertUnits.ToSimUnits(device.Viewport.Width), ConvertUnits.ToSimUnits(device.Viewport.Height), 0f, 0f, 1f);
            var view = Matrix.Identity;

            debugView.BeginCustomDraw(projection, view);
            foreach (Body b in world.BodyList)
            {
                Transform xf;
                b.GetTransform(out xf);

                foreach (Fixture f in b.FixtureList)
                {
                    debugView.DrawShape(f, xf, Color.Black * 0.25f);
                }
            }

            debugView.EndCustomDraw();

            drawWorld(spriteBatch);
            spriteBatch.End();

            base.Draw(spriteBatch);
        }

        public Vector2 ConvertWorldToScreen(Vector2 location)
        {
            Vector3 t = new Vector3(location, 0);
            t = device.Viewport.Project(t, SimProj, SimView, Matrix.Identity);
            return new Vector2(t.X, t.Y);
        }

        public Vector2 ConvertScreenToWorld(Vector2 location)
        {
            Vector3 t = new Vector3(location, 1);
            t = device.Viewport.Unproject(t, SimProj, SimView, Matrix.Identity);
            return new Vector2(t.X, t.Y);
        }
    }
}
