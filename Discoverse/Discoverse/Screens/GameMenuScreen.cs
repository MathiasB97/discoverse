﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discoverse.Systems;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using BreakerUI.Components;

namespace Discoverse.Screens
{
    public class PS4Tile
    {
        string tileTitle;
        Texture2D tileTexture;
        Color tileColor;

        public string Title
        {
            get { return this.tileTitle; }
        }

        public Texture2D Texture
        {
            get { return this.tileTexture; }
        }

        public Color Color
        {
            get { return this.tileColor; }
        }

        public PS4Tile(string title, Texture2D picture, Color color)
        {
            this.tileTitle = title;
            this.tileTexture = picture;
            this.tileColor = color;
        }
    }

    class GameMenuScreen : GameScreen
    {
        SpriteFont font;
        SpriteFont font2;
        Texture2D prefab;

        List<PS4Tile> items;

        int currentItem = 1;

        public override void Init()
        {

            base.Init();
        }

        public override void LoadContent()
        {
            font = ScreenManager.Content.getFont("Express");
            font2 = ScreenManager.Content.getFont("Express2X");
            backgroundColor = Color.FromNonPremultiplied(60, 98, 182, 255);

            prefab = Content.getTexture("prefab");

            items = new List<PS4Tile>();
            items.Add(new PS4Tile("Battlefield 4", Content.addTexture("battlefield4", "Textures/bf4"), Color.Orange));
            items.Add(new PS4Tile("Grand Theft Auto V", Content.addTexture("grandtheftautov", "Textures/gta5"), Color.LightGreen));

            /*Dropdown drop = new Dropdown(new Vector2(100, 100), "Dropdown", UIManager);
            drop.addItemToList("Item 1");
            drop.addItemToList("Item 2");
            drop.addItemToList("Item 3");
            drop.addItemToList("Item 4");
            drop.addItemToList("Item 5");

            Button btn = new Button(new Vector2(100 + drop.Size.X + 64 + 32, 100), "Button", UIManager);
            Textbox txtb = new Textbox(new Vector2(100, 100 + drop.Size.Y + 64), "Textbox", UIManager);*/

            base.LoadContent();
        }

        public override void UnloadContent()
        {
            base.UnloadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            ScreenManager.GraphicsDevice.Clear(this.backgroundColor);
            spriteBatch.Begin();

            string d = "";

            if (DateTime.Now.Hour > 12)
                d = "PM";
            else
                d = "AM";

            spriteBatch.DrawString(font2, DateTime.Now.ToString("H:mm:ss") + " " + d, new Vector2(Dimensions.X - font2.MeasureString(DateTime.Now.ToString("H:mm:ss") + " " + d + " ").X, 0), Color.White);

            for (int i = 0; i < items.Count; i++)
            {
                if (i != currentItem)
                {
                    spriteBatch.Draw(prefab, new Rectangle(160 + (i * 132), 160, 128, 128), items[i].Color * 0.50f);
                    spriteBatch.Draw(items[i].Texture, new Rectangle(160 + (i * 132) + 2, 160 + 2, 128 - 4, 128 - 4), Color.White);
                } else
                {
                    spriteBatch.Draw(prefab, new Rectangle(160 + (i * 132), 160, 128, 128), items[currentItem].Color * 0.50f);
                    spriteBatch.Draw(items[currentItem].Texture, new Rectangle(160 + (i * 132) + 2, 160 + 2, 128 - 4, 128 - 4), Color.White);
                    spriteBatch.DrawString(font, items[currentItem].Title, new Vector2(292, 303), Color.Black * 0.75f);
                }
            }
            spriteBatch.DrawString(font, mousePosition.ToString(), mousePosition, Color.White);

            spriteBatch.End();

            base.Draw(spriteBatch);
        }
    }
}
