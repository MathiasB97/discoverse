﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discoverse.Systems;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Discoverse.Screens
{
    class ScreenTemplate : GameScreen
    {
        SpriteFont font;

        public override void Init()
        {

            base.Init();
        }

        public override void LoadContent()
        {
            backgroundColor = Color.Black;
            font = Content.getFont("Express");

            base.LoadContent();
        }

        public override void UnloadContent()
        {
            base.UnloadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            ScreenManager.GraphicsDevice.Clear(this.backgroundColor);
            spriteBatch.Begin();

            spriteBatch.DrawString(font, "Template", Vector2.Zero, Color.White);

            spriteBatch.End();

            base.Draw(spriteBatch);
        }
    }
}
