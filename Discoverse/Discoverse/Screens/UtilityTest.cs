﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Discoverse.Systems;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections;
using System.Diagnostics;

using MoonSharp.Interpreter;
using System.Reflection;

using BreakerUI;
using BreakerUI.Components;
using System.IO;

namespace Discoverse.Screens
{
    public class UtilityTest : GameScreen
    {
        SpriteFont font1;
        SpriteFont font2;
        Systems.Coroutine coroutine = new Systems.Coroutine();

        public override void Init()
        {
            base.Init();
        }

        Dictionary<string, string> achName;
        Dictionary<string, Texture2D> achievements;

        Dictionary<Texture2D, string> xboxG;
        Dictionary<Texture2D, string> ps4G;

        Dictionary<string, Dictionary<string, Texture2D>> idText;
        Dictionary<string, Texture2D> textIMG;
        string text;

        public override void LoadContent()
        {
            //backgroundColor = Color.FromNonPremultiplied(0, 102, 153, 255);
            backgroundColor = Color.FromNonPremultiplied(24, 25, 22, 255);
            font1 = Content.getFont("Express");
            font2 = Content.getFont("Roboto2X");

            textIMG = new Dictionary<string, Texture2D>();
            idText = new Dictionary<string, Dictionary<string, Texture2D>>();

            xboxG = new Dictionary<Texture2D, string>();
            ps4G = new Dictionary<Texture2D, string>();
            // Xbox
            xboxG.Add(Content.addTexture("XBpad.A", "Textures/Gamepad/360_a"), "A");
            xboxG.Add(Content.addTexture("XBpad.B", "Textures/Gamepad/360_b"), "B");
            xboxG.Add(Content.addTexture("XBpad.X", "Textures/Gamepad/360_x"), "X");
            xboxG.Add(Content.addTexture("XBpad.Y", "Textures/Gamepad/360_y"), "Y");
            xboxG.Add(Content.addTexture("XBpad.LT", "Textures/Gamepad/360_lt"), "LT");
            xboxG.Add(Content.addTexture("XBpad.RT", "Textures/Gamepad/360_rt"), "RT");
            xboxG.Add(Content.addTexture("XBpad.LB", "Textures/Gamepad/360_lb"), "LB");
            xboxG.Add(Content.addTexture("XBpad.RB", "Textures/Gamepad/360_rb"), "RB");
            xboxG.Add(Content.addTexture("XBpad.LS", "Textures/Gamepad/360_ls"), "Left Stick");
            xboxG.Add(Content.addTexture("XBpad.RS", "Textures/Gamepad/360_rs"), "Right Stick");
            xboxG.Add(Content.addTexture("XBpad.DPAD", "Textures/Gamepad/360_dpad"), "D-Pad");
            xboxG.Add(Content.addTexture("XBpad.view", "Textures/Gamepad/360_view"), "View");
            xboxG.Add(Content.addTexture("XBpad.menu", "Textures/Gamepad/360_menu"), "Menu");
            // Playstation
            ps4G.Add(Content.addTexture("PSpad.T", "Textures/Gamepad/PS4_t"), "Triangle");
            ps4G.Add(Content.addTexture("PSpad.C", "Textures/Gamepad/PS4_c"), "Circle");
            ps4G.Add(Content.addTexture("PSpad.X", "Textures/Gamepad/PS4_x"), "Cross");
            ps4G.Add(Content.addTexture("PSpad.S", "Textures/Gamepad/PS4_s"), "Square");
            ps4G.Add(Content.addTexture("PSpad.L1", "Textures/Gamepad/PS4_l1"), "L1");
            ps4G.Add(Content.addTexture("PSpad.R1", "Textures/Gamepad/PS4_r1"), "R1");
            ps4G.Add(Content.addTexture("PSpad.L2", "Textures/Gamepad/PS4_l2"), "L2");
            ps4G.Add(Content.addTexture("PSpad.R2", "Textures/Gamepad/PS4_r2"), "R2");
            ps4G.Add(Content.addTexture("PSpad.L3", "Textures/Gamepad/PS4_l3"), "L3");
            ps4G.Add(Content.addTexture("PSpad.R3", "Textures/Gamepad/PS4_r3"), "R3");
            ps4G.Add(Content.addTexture("PSpad.AL", "Textures/Gamepad/PS4_al"), "Left Stick");
            ps4G.Add(Content.addTexture("PSpad.AR", "Textures/Gamepad/PS4_ar"), "Right Stick");
            ps4G.Add(Content.addTexture("PSpad.DPAD", "Textures/Gamepad/PS4_dpad"), "D-Pad");
            ps4G.Add(Content.addTexture("PSpad.SEL", "Textures/Gamepad/PS4_sel"), "Select");
            ps4G.Add(Content.addTexture("PSpad.START", "Textures/Gamepad/PS4_start"), "Start");

            achName = new Dictionary<string, string>();
            achName.Add("all-seeing-eye", "The Truth");
            achName.Add("amplitude", "Surfer");
            achName.Add("computer-fan", "Up and Running");
            achName.Add("conversation", "Connected");
            achName.Add("cracked-glass", "Falling Apart");
            achName.Add("crowbar", "Tooling Around");
            achName.Add("embrassed-energy", "Spiritual Guide");
            achName.Add("gold-stack", "Pot'o'Gold");
            achName.Add("graduate-cap", "Grade School");
            achName.Add("oat", "Naturalist");
            achName.Add("plug", "Experimenting");
            achName.Add("radar-sweep", "Discovered");
            achName.Add("sattelite", "Up, Up and Away");
            achName.Add("sinusoidal-beam", "Reaching Out");
            achName.Add("vortex", "Traveler");
            achName.Add("watch", "Times Up!");

            achievements = new Dictionary<string, Texture2D>();
            foreach (var file in Directory.GetFiles(Assembly.GetExecutingAssembly().Location.Replace("\\Discoverse.exe", "") + "\\Content\\Textures\\achievements\\"))
            {
                string[] f = file.Split('\\');
                string pname = f[f.Length - 1];
                string name = pname.Replace(".xnb", "");

                if (achName.ContainsKey(name))
                    achievements.Add(name, Content.addTexture("achievement." + name, "Textures/achievements/" + name));
            }

            text = "You have purchased The Hen House. The business will provide drinks to the people of San Andreas.";

            base.LoadContent();
        }
        public override void UnloadContent()
        {
            base.UnloadContent();
        }

        static IEnumerator Pause(float time)
        {
            var watch = Stopwatch.StartNew();
            while (watch.Elapsed.TotalSeconds < time)
                yield return 0;
        }

        int cur = 0;
        public override void Update(GameTime gameTime)
        {
            if (coroutine.Running)
                coroutine.Update();

            if (input.KeyPressed(ScreenManager.keyBinds.keyMappings[MappedActions.Left]) && cur > 0)
                cur -= 1;

            if (input.KeyPressed(ScreenManager.keyBinds.keyMappings[MappedActions.Right]) && cur < (achievements.Count - 1))
                cur += 1;

            if (input.GamepadKeyPressed(ScreenManager.keyBinds.getGamepadBind(Gamepad_Type.Xbox, "Move Left")) && cur > 0)
                cur -= 1;

            if (input.GamepadKeyPressed(ScreenManager.keyBinds.getGamepadBind(Gamepad_Type.Xbox, "Move Right")) && cur < (achievements.Count - 1))
                cur += 1;
            /*if (input.KeyPressed(Keys.A) && cur > 0)
                cur -= 1;

            if (input.KeyPressed(Keys.D) && cur < (achievements.Count - 1))
                cur += 1;*/

            base.Update(gameTime);
        }

        private void handleIMG(string id, int s, string[] words, string dWord, SpriteFont font, string texName)
        {
            Vector2 weigh = Vector2.Zero;
            for (int w = 0; w < s + 1; w++)
            {
                weigh = weigh + new Vector2(font.MeasureString(words[w]).X, 0) - new Vector2(font.MeasureString(dWord).X, 0);
            }
            Vector2 pos = new Vector2(weigh.X + (s * 2) + 2, 23);
            if (!idText.ContainsKey(id))
            {
                Dictionary<string, Texture2D> idIMG = new Dictionary<string, Texture2D>();
                idIMG.Add(pos.X.ToString() + ";" + pos.Y.ToString(), Content.getTexture(texName));
                idText.Add(id, idIMG);
            } else
            {
                if (!idText[id].ContainsKey(pos.X.ToString() + ";" + pos.Y.ToString()))
                    idText[id].Add(pos.X.ToString() + ";" + pos.Y.ToString(), Content.getTexture(texName));
            }
            /*if (!textIMG.ContainsKey(pos.X.ToString() + ";" + pos.Y.ToString()))
                textIMG.Add(pos.X.ToString() + ";" + pos.Y.ToString(), Content.getTexture(texName));*/
        }

        private string WrapText(SpriteFont font, string text)
        {
            string[] words = text.Split(' ');
            StringBuilder sb = new StringBuilder();
            float linewidth = 0f;
            float maxLine = 315f; //a bit smaller than the box so you can have some padding...etc
            float spaceWidth = font.MeasureString(" ").X;

            for (int s = 0; s < words.Length; s++)
            {
                string dWord = words[s];

                switch (dWord)
                {
                    // Xbox Keys
                    case "~XB1.A~":
                        {
                            dWord = "    ";
                            handleIMG(text, s, words, dWord, font, "XBpad.A");
                            break;
                        }
                    case "~XB1.B~":
                        {
                            dWord = "    ";
                            handleIMG(text, s, words, dWord, font, "XBpad.B");
                            break;
                        }
                    case "~XB1.X~":
                        {
                            dWord = "    ";
                            handleIMG(text, s, words, dWord, font, "XBpad.X");
                            break;
                        }
                    case "~XB1.Y~":
                        {
                            dWord = "    ";
                            handleIMG(text, s, words, dWord, font, "XBpad.Y");
                            break;
                        }
                    // Playstation Buttons
                    case "~PS4.X~":
                        {
                            dWord = "    ";
                            handleIMG(text, s, words, dWord, font, "PSpad.X");
                            break;
                        }
                    case "~PS4.S~":
                        {
                            dWord = "    ";
                            handleIMG(text, s, words, dWord, font, "PSpad.S");
                            break;
                        }
                    case "~PS4.C~":
                        {
                            dWord = "    ";
                            handleIMG(text, s, words, dWord, font, "PSpad.C");
                            break;
                        }
                    case "~PS4.T~":
                        {
                            dWord = "    ";
                            handleIMG(text, s, words, dWord, font, "PSpad.T");
                            break;
                        }
                    default:
                        {
                            dWord = dWord;
                            break;
                        }

                }

                Vector2 size = font.MeasureString(dWord);
                if (linewidth + size.X < maxLine)
                {
                    sb.Append(dWord + " ");
                    linewidth += size.X + spaceWidth;
                }
                else
                {
                    sb.Append("\n" + dWord + " ");
                    linewidth = size.X + spaceWidth;
                }
            }
            return sb.ToString();
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            ScreenManager.GraphicsDevice.Clear(this.backgroundColor);
            spriteBatch.Begin();

            /*string lString = "Discoverse Achievements";
            spriteBatch.Draw(Content.getTexture("prefab"), new Rectangle((1280 - 500) / 2, (720 - (int)font2.MeasureString(lString).Y) / 4, 500, (int)font2.MeasureString(lString).Y), Color.Black * 0.5f);
            spriteBatch.DrawString(font2, lString, new Vector2((1280 - (int)font2.MeasureString(lString).X) / 2, (720 - (int)font2.MeasureString(lString).Y - 16) / 4), Color.White * 0.75f);*/

            Texture2D logo = Content.getTexture("logo");
            spriteBatch.Draw(logo, new Vector2((1280 - logo.Width) / 2, (720 - logo.Height) / 10), Color.White);
            spriteBatch.DrawString(font2, "Trophies", new Vector2((1280 - (int)font2.MeasureString("Trothies").X) / 2, (720 - (int)font2.MeasureString("Trophies").Y - 16) / 4 - 20), Color.White);

            int size = 256;
            if (cur > 1)
                spriteBatch.Draw(achievements.ElementAt(cur - 2).Value, new Rectangle((1280 - size) / 2 - 128 - 32, (720 - size) / 2 + 48, size - 64 - 32, size - 64 - 32), Color.White * 0.15f); // Previous Prev Trophy
            if (cur > 0)
            {
                spriteBatch.Draw(achievements.ElementAt(cur - 1).Value, new Rectangle((1280 - size) / 2 - 64 - 32, (720 - size) / 2 + 32, size - 64, size - 64), Color.White * 0.75f); // Previous Trophy
                spriteBatch.DrawString(font1, achName[achievements.ElementAt(cur - 1).Key], new Vector2((1280 - (int)font1.MeasureString(achName[achievements.ElementAt(cur - 1).Key]).X) / 2 - 164, (720 - size) / 2 + size - 24), Color.White * 0.25f);
            }
            if (cur < (achievements.Count - 2))
                spriteBatch.Draw(achievements.ElementAt(cur + 2).Value, new Rectangle((1280 - size) / 2 + 256, (720 - size) / 2 + 48, size - 64 - 32, size - 64 - 32), Color.White * 0.15f); // Next Nex Trophy
            if (cur < (achievements.Count - 1))
            {
                spriteBatch.Draw(achievements.ElementAt(cur + 1).Value, new Rectangle((1280 - size) / 2 + 128 + 32, (720 - size) / 2 + 32, size - 64, size - 64), Color.White * 0.75f); // Next Trophy
                spriteBatch.DrawString(font1, achName[achievements.ElementAt(cur + 1).Key], new Vector2((1280 - (int)font1.MeasureString(achName[achievements.ElementAt(cur + 1).Key]).X) / 2 + 164, (720 - size) / 2 + size - 24), Color.White * 0.25f);
            }
            try
            {
                spriteBatch.Draw(Content.getTexture("prefab"), new Rectangle((1280 - size) / 2 - 2, (720 - size) / 2 - 2, size + 4, size + 4), Color.White * 0.75f); // Current Trophy
                spriteBatch.Draw(achievements.ElementAt(cur).Value, new Rectangle((1280 - size) / 2, (720 - size) / 2, size, size), Color.White); // Current Trophy
                spriteBatch.DrawString(font2, achName[achievements.ElementAt(cur).Key], new Vector2((1280 - (int)font2.MeasureString(achName[achievements.ElementAt(cur).Key]).X) / 2, (720 - size) / 2 + size + 12), Color.White);
            }
            catch
            {

            }

            // Xbox
            for (int i = 0; i < xboxG.Count; i++)
            {
                var button = xboxG.ElementAt(i);
                spriteBatch.Draw(button.Key, new Rectangle(10, 10 + (i * (31 + 4)), 32, 31), Color.White);
                spriteBatch.DrawString(font1, button.Value, new Vector2(32 + 10, 14 + (i * (31 + 4))), Color.White);
            }
            // Playstation
            for (int i = 0; i < ps4G.Count; i++)
            {
                var button = ps4G.ElementAt(i);
                spriteBatch.Draw(button.Key, new Rectangle(128, 10 + (i * (31 + 4)), 32, 31), Color.White);
                spriteBatch.DrawString(font1, button.Value, new Vector2(32 + 128, 14 + (i * (31 + 4))), Color.White);
            }

            drawIS(spriteBatch, text, new Vector2(25, 25));

            spriteBatch.End();

            base.Draw(spriteBatch);
        }

        void drawIS(SpriteBatch spriteBatch, string wstring, Vector2 position)
        {
            Vector2 pos = font1.MeasureString(WrapText(font1, wstring));
            spriteBatch.Draw(Content.getTexture("prefab"), new Rectangle((int)position.X - 8, (int)position.Y - 4, (int)pos.X + 16, (int)pos.Y + 8), Color.Black * 0.75f);
            spriteBatch.DrawString(font1, WrapText(font1, wstring), position, Color.White);

            int bsa = 0;
            foreach (KeyValuePair<string, Dictionary<string, Texture2D>> data in idText)
            {
                foreach (KeyValuePair<string, Texture2D> innerData in data.Value)
                {
                    bsa += 1;
                    spriteBatch.DrawString(font1, innerData.Key, new Vector2(400, 0 + (bsa * 20)), Color.White);
                    //spriteBatch.Draw(innerData.Value, new Vector2(400, 0 + (bsa * 70)), Color.White);
                    string[] d = innerData.Key.Split(';');
                    Vector2 dP = new Vector2(Convert.ToInt32(d[0]), Convert.ToInt32(d[1]));
                    spriteBatch.Draw(innerData.Value, new Rectangle((int)dP.X, (int)dP.Y, 24, 23), Color.White);
                }
            }

            /*foreach (KeyValuePair<string, Texture2D> data in textIMG)
            {
                string[] d = data.Key.Split(';');
                Vector2 dP = new Vector2(Convert.ToInt32(d[0]), Convert.ToInt32(d[1]));
                spriteBatch.Draw(data.Value, new Rectangle((int)dP.X, (int)dP.Y, 24, 23), Color.White);
            }*/
        }
    }
}
