﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

using FarseerPhysics;
using FarseerPhysics.Collision;
using FarseerPhysics.Collision.Shapes;
using FarseerPhysics.Common;
using FarseerPhysics.Common.ConvexHull;
using FarseerPhysics.Common.Decomposition;
using FarseerPhysics.Common.PhysicsLogic;
using FarseerPhysics.Common.PolygonManipulation;
using FarseerPhysics.Common.TextureTools;
using FarseerPhysics.Controllers;
using FarseerPhysics.DebugView;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using FarseerPhysics.Dynamics.Joints;
using FarseerPhysics.Factories;

using Discoverse.Systems;
using Discoverse.Systems.Mechanics;
using Discoverse.Components;
using System.IO;
using System.Reflection;
using System.Xml;
using BreakerUI.Components;

namespace Discoverse.Screens
{
    public class WorldScreen : GameScreen
    {
        SpriteFont font;

        World world;

        DebugViewXNA debugView;

        List<Body> bodies;
        CircleShape circle;
        Fixture fixture;

        public override void Init()
        {

            base.Init();
        }

        public override void LoadContent()
        {
            backgroundColor = Color.CornflowerBlue;
            font = Content.getFont("Express");

            bodies = new List<Body>();

            world = new World(new Vector2(0f, 9.82f));
            // 1 meter = 64 pixels
            ConvertUnits.SetDisplayUnitToSimUnitRatio(64f);

            /*body = BodyFactory.CreateRectangle(world, ConvertUnits.ToSimUnits(20f), ConvertUnits.ToSimUnits(20f), 10f);
            body.Position = ConvertUnits.ToSimUnits(500f, 300f);
            body.BodyType = BodyType.Static;*/
            for (int i = 0; i < 60; i++)
            {
                for (int j = 0; j < 40; j++)
                {
                    Body body = BodyFactory.CreateRectangle(world, ConvertUnits.ToSimUnits(20f), ConvertUnits.ToSimUnits(20f), 10f);
                    body.Position = ConvertUnits.ToSimUnits(50f + (i * 20f), 30f + (j * 20f));
                    body.BodyType = BodyType.Static;
                }
            }

            // Setup Debug View
            if (debugView == null)
            {
                debugView = new DebugViewXNA(world);
                debugView.AppendFlags(DebugViewFlags.Shape);
                /*debugView.RemoveFlags(DebugViewFlags.Controllers);
                debugView.RemoveFlags(DebugViewFlags.Joint);*/
                debugView.LoadContent(device, Content.ContentManager());
                debugView.Enabled = true;
            }

            base.LoadContent();
        }

        public override void UnloadContent()
        {
            base.UnloadContent();
        }

        public override void Update(GameTime gameTime)
        {
            world.Step(0.0333333f);

            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            ScreenManager.GraphicsDevice.Clear(this.backgroundColor);

            var projection = Matrix.CreateOrthographicOffCenter(0f, ConvertUnits.ToSimUnits(device.Viewport.Width), ConvertUnits.ToSimUnits(device.Viewport.Height), 0f, 0f, 1f);
            var view = Matrix.Identity;

            spriteBatch.Begin();

            debugView.BeginCustomDraw(projection, view);
            foreach (Body b in world.BodyList)
            {
                Transform xf;
                b.GetTransform(out xf);

                foreach (Fixture f in b.FixtureList)
                {
                    debugView.DrawShape(f, xf, Color.Black * 0.25f);
                }
            }
            debugView.EndCustomDraw();

            spriteBatch.End();

            base.Draw(spriteBatch);
        }
    }
}
