﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discoverse.Systems;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Discoverse.Screens
{
    public class WorldTest : GameScreen
    {
        SpriteFont font;

        Texture2D prefab;

        public static Tile[,] worldTiles;
        List<Tile> screenTiles;

        bool worldLoaded = false;
        int worldW, worldH;
        int startingHeight;
        int airCount = 0;
        int dirtCount = 0;
        int cobbleCount = 0;
        int stoneCount = 0;
        int bronzeCount = 0;
        int ironCount = 0;
        int coalCount = 0;
        int silverCount = 0;
        int goldCount = 0;
        int diamondCount = 0;

        float elapsedTime = 0f;

        Random gen = new Random();

        Texture2D zelda;

        public override void Init()
        {

            base.Init();
        }

        public override void LoadContent()
        {
            backgroundColor = Color.Black;
            font = Content.getFont("Express");
            prefab = Content.getTexture("prefab");

            zelda = Content.addTexture("zelda_sheet", "Textures/zeldasheet");

            BaseWorld world = new BaseWorld(128, 128);
            bool result;
            if (world.elements["tiles"] != null)
            {
                float[][] tiles = world.elements["tiles"];
                worldTiles = new Tile[tiles.Length, tiles[0].Length];
                worldW = tiles.Length;
                worldH = tiles[0].Length;

                int number = (int)((float)worldH * 0.33f);

                float number2 = 0.01f;
                int number3 = 30; // Dirt
                int number4 = 60; // Stone
                int number5 = 10; // End

                for (int i = 0; i < tiles.Length; i++)
                {
                    for (int j = 0; j < tiles[0].Length; j++)
                    {
                        if (j < number)
                        {
                            WorldTest.worldTiles[j, i] = new emptyTile();
                            WorldTest.worldTiles[j, i].position = new Vector2((float)(i * 16), (float)(j * 16));
                        }
                        float number6;
                        if (j < number + number3)
                            number6 = 0.18f;
                        else
                            number6 = number2;

                        if (tiles[j][i] > number6)
                        {
                            if ((float)j >= (float)number + tiles[j][i] * 8f && (float)j < (float)(number + number3) + tiles[j][i] * 32f)
                            {
                                WorldTest.worldTiles[j, i] = new dirtTile();
                                WorldTest.worldTiles[j, i].position = new Vector2((float)(i * 16), (float)(j * 16));
                            }
                            else if ((float)j > (float)(number + number3) + tiles[j][i] * 32f)
                            {
                                WorldTest.worldTiles[j, i] = new cobbleTile();
                                WorldTest.worldTiles[j, i].position = new Vector2((float)(i * 16), (float)(j * 16));
                            }
                            else if ((float)j < (float)number + tiles[j][i] * 8f)
                            {
                                WorldTest.worldTiles[j, i] = new emptyTile();
                                WorldTest.worldTiles[j, i].position = new Vector2((float)(i * 16), (float)(j * 16));
                            }
                            if (tiles[j][i] > 0.8f)
                            {
                                if ((float)j > (float)(number + number4) + tiles[j][i] * 32f)
                                {
                                    WorldTest.worldTiles[j, i] = new stoneTile();
                                    WorldTest.worldTiles[j, i].position = new Vector2((float)(i * 16), (float)(j * 16));
                                }
                            }
                        } else
                        {
                            worldTiles[j, i] = new emptyTile();
                            worldTiles[j, i].position = new Vector2((float)(i * 16), (float)(j * 16));
                        }

                        if (j >= this.worldH - number5)
                        {
                            WorldTest.worldTiles[j, i] = new abyssalTile();
                            WorldTest.worldTiles[j, i].position = new Vector2((float)(i * 16), (float)(j * 16));
                        }
                    }
                }

                for (int i = 0; i < tiles.Length; i++)
                {
                    for (int j = 0; j < tiles[0].Length; j++)
                    {
                        if (WorldTest.worldTiles[j, i].type == "dirt")
                        {
                            if (this.WorldIndexExists(new Point(j - 1, i)))
                            {
                                if (WorldTest.worldTiles[j - 1, i].type == "blank")
                                {
                                    WorldTest.worldTiles[j, i].material = Content.getTexture("prefab");
                                }
                            }
                        }
                        if (j == (int)((float)number + tiles[j][i] * 8f))
                        {
                            if (this.WorldIndexExists(new Point(j + 1, i)))
                            {
                                if (WorldTest.worldTiles[j + 1, i].type == "dirt")
                                {
                                    if (i == 0)
                                    {
                                        this.startingHeight = j - 10;
                                    }
                                    if (this.gen.Next(6) == 0)
                                    {
                                        //Element.trees.Add(new Tree(new Point(i, j), this.gen.Next(4, 12)));
                                    }
                                }
                            }
                        }
                    }
                }

                Rectangle rectangle = new Rectangle(this.gen.Next(300), number + 60 + this.gen.Next(35), 375, 350);
                Rectangle rectangle2 = new Rectangle(450 + this.gen.Next(300), number + 60 + this.gen.Next(35), 400, 325);
                Rectangle rectangle3 = new Rectangle(this.gen.Next(150), number + 150 + this.gen.Next(50), 300, 325);
                Rectangle rectangle4 = new Rectangle(400 + this.gen.Next(150), number + 100 + this.gen.Next(50), 275, 250);
                Rectangle rectangle5 = new Rectangle(this.gen.Next(50), number + 175 + this.gen.Next(100), 225, 175);
                Rectangle rectangle6 = new Rectangle(600 + this.gen.Next(300), number + 125 + this.gen.Next(50), 250, 250);
                Rectangle rectangle7 = new Rectangle(300 + this.gen.Next(150), number + 175 + this.gen.Next(50), 175, 250);
                Rectangle rectangle8 = new Rectangle(this.gen.Next(100), number + 350 + this.gen.Next(50), 175, 125);
                Rectangle rectangle9 = new Rectangle(600 + this.gen.Next(300), number + 400 + this.gen.Next(75), 200, 125);
                Rectangle rectangle10 = new Rectangle(300 + this.gen.Next(150), number + 400 + this.gen.Next(50), 175, 100);
                Rectangle rectangle11 = new Rectangle(this.gen.Next(100), number + 475 + this.gen.Next(50), 175, 125);
                Rectangle rectangle12 = new Rectangle(600 + this.gen.Next(300), number + 475 + this.gen.Next(75), 200, 125);
                Rectangle rectangle13 = new Rectangle(300 + this.gen.Next(150), number + 475 + this.gen.Next(50), 150, 100);
                Rectangle rectangle14 = new Rectangle(this.gen.Next(100), number + 600 + this.gen.Next(25), 200, 75);
                Rectangle rectangle15 = new Rectangle(600 + this.gen.Next(200), number + 600 + this.gen.Next(40), 225, 100);
                Rectangle rectangle16 = new Rectangle(300 + this.gen.Next(150), number + 600 + this.gen.Next(30), 150, 50);
                if (world.elements["materials"] != null)
                {
                    for (int i = 0; i < world.elements["materials"].Length; i++)
                    {
                        for (int j = 0; j < world.elements["materials"].Length; j++)
                        {
                            if (world.elements["bronze"][i][j] > 0.962f)
                            {
                                if (rectangle.Contains(new Point(i, j)))
                                {
                                    WorldTest.worldTiles[j, i] = new bronzeTile();
                                    WorldTest.worldTiles[j, i].position = new Vector2((float)(i * 16), (float)(j * 16));
                                }
                                else if (rectangle2.Contains(new Point(i, j)))
                                {
                                    WorldTest.worldTiles[j, i] = new bronzeTile();
                                    WorldTest.worldTiles[j, i].position = new Vector2((float)(i * 16), (float)(j * 16));
                                }
                            }
                            if (world.elements["iron"][i][j] > 0.958f)
                            {
                                if (rectangle3.Contains(new Point(i, j)))
                                {
                                    WorldTest.worldTiles[j, i] = new ironTile();
                                    WorldTest.worldTiles[j, i].position = new Vector2((float)(i * 16), (float)(j * 16));
                                }
                                else if (rectangle4.Contains(new Point(i, j)))
                                {
                                    WorldTest.worldTiles[j, i] = new ironTile();
                                    WorldTest.worldTiles[j, i].position = new Vector2((float)(i * 16), (float)(j * 16));
                                }
                            }
                            if (world.elements["coal"][i][j] > 0.96f)
                            {
                                if (rectangle5.Contains(new Point(i, j)))
                                {
                                    WorldTest.worldTiles[j, i] = new coalTile();
                                    WorldTest.worldTiles[j, i].position = new Vector2((float)(i * 16), (float)(j * 16));
                                }
                                else if (rectangle6.Contains(new Point(i, j)))
                                {
                                    WorldTest.worldTiles[j, i] = new coalTile();
                                    WorldTest.worldTiles[j, i].position = new Vector2((float)(i * 16), (float)(j * 16));
                                }
                                else if (rectangle7.Contains(new Point(i, j)))
                                {
                                    WorldTest.worldTiles[j, i] = new coalTile();
                                    WorldTest.worldTiles[j, i].position = new Vector2((float)(i * 16), (float)(j * 16));
                                }
                            }
                            if (world.elements["silver"][i][j] > 0.967f)
                            {
                                if (rectangle8.Contains(new Point(i, j)))
                                {
                                    WorldTest.worldTiles[j, i] = new silverTile();
                                    WorldTest.worldTiles[j, i].position = new Vector2((float)(i * 16), (float)(j * 16));
                                }
                                else if (rectangle9.Contains(new Point(i, j)))
                                {
                                    WorldTest.worldTiles[j, i] = new silverTile();
                                    WorldTest.worldTiles[j, i].position = new Vector2((float)(i * 16), (float)(j * 16));
                                }
                                else if (rectangle10.Contains(new Point(i, j)))
                                {
                                    WorldTest.worldTiles[j, i] = new silverTile();
                                    WorldTest.worldTiles[j, i].position = new Vector2((float)(i * 16), (float)(j * 16));
                                }
                            }
                            if (world.elements["gold"][i][j] > 0.967f)
                            {
                                if (rectangle11.Contains(new Point(i, j)))
                                {
                                    WorldTest.worldTiles[j, i] = new goldTile();
                                    WorldTest.worldTiles[j, i].position = new Vector2((float)(i * 16), (float)(j * 16));
                                }
                                else if (rectangle12.Contains(new Point(i, j)))
                                {
                                    WorldTest.worldTiles[j, i] = new goldTile();
                                    WorldTest.worldTiles[j, i].position = new Vector2((float)(i * 16), (float)(j * 16));
                                }
                                else if (rectangle13.Contains(new Point(i, j)))
                                {
                                    WorldTest.worldTiles[j, i] = new goldTile();
                                    WorldTest.worldTiles[j, i].position = new Vector2((float)(i * 16), (float)(j * 16));
                                }
                            }
                            if (world.elements["diamonds"][i][j] > 0.97f)
                            {
                                if (rectangle14.Contains(new Point(i, j)))
                                {
                                    WorldTest.worldTiles[j, i] = new diamondTile();
                                    WorldTest.worldTiles[j, i].position = new Vector2((float)(i * 16), (float)(j * 16));
                                }
                                else if (rectangle15.Contains(new Point(i, j)))
                                {
                                    WorldTest.worldTiles[j, i] = new diamondTile();
                                    WorldTest.worldTiles[j, i].position = new Vector2((float)(i * 16), (float)(j * 16));
                                }
                                else if (rectangle16.Contains(new Point(i, j)))
                                {
                                    WorldTest.worldTiles[j, i] = new diamondTile();
                                    WorldTest.worldTiles[j, i].position = new Vector2((float)(i * 16), (float)(j * 16));
                                }
                            }
                        }
                    }
                }
            }

            base.LoadContent();
        }

        public bool WorldIndexExists(Point gridLocation)
        {
            return gridLocation.X >= 0 && gridLocation.X < this.worldW && gridLocation.Y >= 0 && gridLocation.Y < this.worldH;
        }

        public override void UnloadContent()
        {
            base.UnloadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            ScreenManager.GraphicsDevice.Clear(this.backgroundColor);
            spriteBatch.Begin();

            spriteBatch.End();

            base.Draw(spriteBatch);
        }
    }
}
