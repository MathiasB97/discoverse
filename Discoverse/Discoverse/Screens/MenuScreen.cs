﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Discoverse.Systems;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections;
using System.Diagnostics;

using System.Reflection;

using BreakerUI;
using BreakerUI.Components;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;

namespace Discoverse.Screens
{
    enum ValidityCheck
    {
        UpdateCheck,
        DLC_Check,
        PiracyCheck,
        LoginCheck,
        Passed,
        Failed,
    }

    class MenuScreen : GameScreen
    {
        SpriteFont font;
        Texture2D logo;

        Coroutine routine;

        /*ParticleSystem pSystem;
        int particles = 0;*/

        Menu menu;

        ValidityCheck stepProcess;
        string stepString = "";
        int time = 0;

        string keyAttempt = "";
        bool validKey = false;

        public override void Init()
        {

            base.Init();
        }

        string sqlKey = string.Empty;
        BreakerUI.Components.Textbox key;

        public override void LoadContent()
        {
            //backgroundColor = Color.FromNonPremultiplied(24, 25, 22, 255);
            backgroundColor = Color.FromNonPremultiplied(100, 100, 100, 255);
            //backgroundColor = Color.White;
            font = Content.getFont("Roboto");
            logo = Content.getTexture("logo");

            routine = new Coroutine();
            routine.Start(Validity());

            Notifications.onNotificationRemoved += Notifications_onNotificationRemoved;

            menu = new Menu(new Vector2(100, 525), false);
            menu.addMenuItem("Story");
            menu.addMenuItem("Achievements");
            menu.addMenuItem("Extras");
            menu.addMenuItem("Options");
            menu.addMenuItem("Quit");
            menu.onMenuItemClick += Menu_onMenuItemClick;

            stepProcess = ValidityCheck.UpdateCheck;
            stepString = "Checking for updates";

            // Test Key = DISCMFIIRYPHDXBAPGXUWJRI
            if (sqliteManager.getTableData("program", "product_key") != "")
                sqlKey = sqliteManager.getTableData("program", "product_key");

            key = new Textbox(new Vector2((1280 - (int)font.MeasureString(stepString).X) / 4, (720 - (int)font.MeasureString(stepString).Y - 16) / 2 + 164), "Product Key", UIManager, ' ', 24);
            key.onInputUpdate += Key_onInputUpdate;
            key.enabled = false;
            Content.addTexture("tick", "Textures/icons/checkmark");
            foreach (Thread t in ScreenManager.threadManager.threads)
                Console.WriteLine(t.Name);

            Notifications.addNotification("Use ~PS4.LS~ to rappel.\nJump by pressing ~PS4.X~ to descend faster. Hold ~PS4.X~ after jumping for a longer jump.", 0, "exampleWrap");

            base.LoadContent();
        }

        private void Notifications_onNotificationRemoved(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
            foreach (MenuItem items in menu.items)
            {
                switch (items.itemName)
                {
                    case "Quit":
                        if (!Notifications.doesNotificationExist("quitMSG"))
                            if (items.designatedColor != Color.Transparent)
                                items.designatedColor = Color.Transparent;
                        break;
                }
            }
        }

        private void Menu_onMenuItemClick(object sender, EventArgs e)
        {
            MenuItem item = sender as MenuItem;
            //System.Windows.Forms.MessageBox.Show("MenuScreen: " + item.itemName);
            NotificationItem nItem;
            switch (item.itemName)
            {
                case "Story":
                    if (!Notifications.doesNotificationExist("storyMSG"))
                        Notifications.addNotification("This feature is currently unavailable due to being under development.", 2, "storyMSG");
                    else
                        Notifications.removeNotification("storyMSG");
                    break;
                case "Achievements":
                    ScreenManager.AddScreen(new Screens.AchievementViewer());
                    ScreenManager.RemoveScreen(this);
                    break;
                case "Options":
                    if (!Notifications.doesNotificationExist("optMSG"))
                        Notifications.addNotification("This feature is currently unavailable due to being under development.", 2, "optMSG");
                    else
                        Notifications.removeNotification("optMSG");
                    break;
                case "Extras":
                    if (!Notifications.doesNotificationExist("bonusCont"))
                        if (sqliteManager.getTableData("program", "bonusContentNotificationSeen") == string.Empty)
                        {
                            sqliteManager.setTableData("program", "bonusContentNotificationSeen", "true");
                            Notifications.addNotification("Thank you for purchasing the limited edition of Discoverse. As a thank you we've included some bonus content.\nThe bonus content can be found under the 'Extras' menu.", 5, "bonusCont");
                            if (!Steam.getAchievement("ACH_WIN_ONE_GAME"))
                                Steam.addAchievement("ACH_WIN_ONE_GAME");
                        } else
                        {
                            /*ScreenManager.AddScreen(new Screens.UtilityTest());
                            ScreenManager.RemoveScreen(this);*/
                            if (!Notifications.doesNotificationExist("extMSG"))
                                Notifications.addNotification("This feature is currently unavailable due to being under development.", 2, "extMSG");
                            else
                                Notifications.removeNotification("extMSG");
                        }
                    else
                        Notifications.removeNotification("bonusCont");
                    break;
                case "Quit":
                    if (!Notifications.doesNotificationExist("quitMSG"))
                    {
                        Notifications.addNotification("To confirm that you wish to quit, please press quit again.", 2, "quitMSG");
                        item.designatedColor = Color.FromNonPremultiplied(194, 81, 87, 255);
                    }
                    else
                        ScreenManager.Game.Exit();
                    break;
            }
        }

        private void Key_onInputUpdate(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
            BreakerUI.Components.Textbox s = sender as BreakerUI.Components.Textbox;
            // Good Green: 72,133,72
            //System.Windows.Forms.MessageBox.Show(s.Text);
            if (s.Text.ToUpper().Contains("DISC") && s.Text.Length == 24)
            {
                keyAttempt = s.Text.ToUpper().Replace("DISC", "");
                if (ScreenManager.breakerNet.Network.checkProductKey(keyAttempt, "discoverse"))
                {
                    validKey = true;
                    sqliteManager.setTableData("program", "product_key", s.Text.ToUpper());
                }
                else
                    validKey = false;
            }
            else if (s.Text.ToUpper().Contains("DISE") && s.Text.Length == 24)
            {
                keyAttempt = s.Text.ToUpper().Replace("DISE", "");
                validKey = true;
                sqliteManager.setTableData("program", "product_key", s.Text.ToUpper());
            }
            else
            {
                keyAttempt = s.Text.ToUpper();
                validKey = false;
            }
        }

        public override void UnloadContent()
        {
            base.UnloadContent();
        }

        void login()
        {
            ScreenManager.breakerNet.Network.attemptLogin("LilDolla", "Mathiastb97");
        }

        IEnumerator Validity()
        {
            while (true)
            {
                switch (stepProcess)
                {
                    case ValidityCheck.UpdateCheck:
                        stepString = "Checking for Updates";
                        if (time <= 3)
                        {
                            time += 1;
                        }
                        else
                        {
                            stepProcess = ValidityCheck.LoginCheck;
                            time = 0;
                        }
                        break;
                    case ValidityCheck.LoginCheck:
                        stepString = "Logging in";

                        if (time <= 3)
                            time += 1;
                        else
                            time = 0;

                        while (ScreenManager.breakerNet.Network.LoggedIn != true)
                        {
                            Thread t = ScreenManager.threadManager.assignWork(login);
                            t.Abort();
                        }

                        if (ScreenManager.breakerNet.Network.LoggedIn)
                        {
                            sqliteManager.setTableData("program", "username", ScreenManager.breakerNet.Network.Username);
                            stepProcess = ValidityCheck.PiracyCheck;
                        }

                        break;
                    case ValidityCheck.PiracyCheck:
                        //stepString = "Checking license";
                        if (sqlKey == string.Empty)
                        {
                            if (validKey)
                            {
                                stepProcess = ValidityCheck.DLC_Check;
                                if (key.enabled)
                                    key.enabled = false;
                            }
                            else
                            {
                                if (time <= 3)
                                    time += 1;
                                else
                                    time = 0;

                                stepString = "Please enter a valid product key";
                                if (!key.enabled)
                                    key.enabled = true;
                            }
                        }
                        else {
                            stepString = "Validating product key";
                            if (sqlKey.ToUpper().Contains("DISC") && sqlKey.Length == 24)
                            {
                                keyAttempt = sqlKey.ToUpper().Replace("DISC", "");
                                if (ScreenManager.breakerNet.Network.checkProductKey(keyAttempt, "discoverse"))
                                    stepProcess = ValidityCheck.DLC_Check;
                                else
                                    stepProcess = ValidityCheck.Failed;
                            }
                            else if (sqlKey.ToUpper().Contains("DISE") && sqlKey.Length == 24)
                            {
                                keyAttempt = sqlKey.ToUpper().Replace("DISE", "");
                                stepProcess = ValidityCheck.Passed;
                            }
                            else
                            {
                                keyAttempt = sqlKey.ToUpper();
                                stepProcess = ValidityCheck.Failed;
                            }
                        }
                        break;
                    case ValidityCheck.DLC_Check:
                        stepString = "Checking for Downloadable Content";
                        if (time <= 3)
                        {
                            time += 1;
                        }
                        else
                        {
                            stepProcess = ValidityCheck.Passed;
                            time = 0;
                            stepString = "";
                            if (!menu.enabled)
                            {
                                menu.enabled = true;
                                //Notifications.addNotification("Nickname: " + ScreenManager.breakerNet.Username + "\nKey: " + ScreenManager.breakerNet.ProductKey, 5);
                                if (!Steam.getAchievement("ACH_TRAVEL_FAR_SINGLE"))
                                    Steam.addAchievement("ACH_TRAVEL_FAR_SINGLE");
                            }
                        }
                        break;
                }

                string dots = "";
                for (int i = 0; i < time; i++)
                {
                    dots = dots + ".";
                }
                stepString = stepString + dots;

                yield return Pause(0.5f);
            }
        }

        static IEnumerator Pause(float time)
        {
            var watch = Stopwatch.StartNew();
            while (watch.Elapsed.TotalSeconds < time)
                yield return 0;
        }

        public override void Update(GameTime gameTime)
        {
            if (routine.Running)
                routine.Update();

            /*pSystem.Update();

            if (input.KeyDown(Keys.A))
                if (particles >= 0)
                    particles -= 10;

            if (input.KeyDown(Keys.D))
                if (particles <= 500)
                    particles += 10;

            if (pSystem.particleCount != particles)
                pSystem.particleCount = particles;*/

            menu.Update(gameTime);

            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            ScreenManager.GraphicsDevice.Clear(this.backgroundColor);
            spriteBatch.Begin();

            //pSystem.Draw(spriteBatch);

            //spriteBatch.DrawString(font, "Game Menu", Vector2.Zero, Color.White);

            spriteBatch.Draw(logo, new Vector2((1280 - logo.Width) / 10, (720 - logo.Height) / 2), Color.White);
            spriteBatch.DrawString(font, stepString, new Vector2((1280 - (int)font.MeasureString(stepString).X) / 4, (720 - (int)font.MeasureString(stepString).Y - 16) / 2 + 128), Color.White);

            if (key.enabled && validKey)
            {
                spriteBatch.Draw(Content.getTexture("tick"), new Rectangle((int)key.Position.X + (int)key.Size.X + 12, (int)key.Position.Y, 24, 24), Color.LimeGreen);
            }

            menu.Draw(spriteBatch);

            spriteBatch.End();

            base.Draw(spriteBatch);
        }
    }
}
