﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discoverse.Systems;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.IO;

namespace Discoverse.Screens
{
    class ProcTest : GameScreen
    {
        SpriteFont font;

        List<string> connectionLog;
        string DLC = "";

        Texture2D prefab;

        int smoothOctave = 2;
        int perlinOctave = 10;

        int maps2make = 10;
        List<Texture2D> maps;
        List<string> mapsData;
        int index = 0;
        List<float[][]> noiseMaps;
        List<float[][]> smoothMaps;
        List<float[][]> perlinMaps;
        Texture2D lastMap;

        public override void Init()
        {

            base.Init();
        }

        public Texture2D createTexture(int sizeX = 128, int sizeY = 128)
        {
            float[][] bNoise = Noise.GenerateWhiteNoise(sizeX, sizeY);
            float[][] sNoise = Noise.GenerateSmoothNoise(bNoise, smoothOctave);
            float[][] pNoise = Noise.GeneratePerlinNoise(sNoise, perlinOctave);

            noiseMaps.Add(bNoise);
            smoothMaps.Add(sNoise);
            perlinMaps.Add(pNoise);
            //float[][] pNoise = Noise.GenerateSeedNoise(128, 128, "test", perlinOctave);
            Color[][] color = Noise.MapGradient(Color.White, Color.Black, pNoise);
            int width = color.Length;
            int height = color[0].Length;
            Color[] textureColors = new Color[width * height];

            Texture2D p = new Texture2D(ScreenManager.GraphicsDevice, width, height);
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    float e = pNoise[i][j];

                    //textureColors[j * height + i] = color[i][j];

                    if (pNoise[i][j] <= 0.3f)
                        textureColors[j * height + i] = Color.LightGreen;
                    else if (pNoise[i][j] >= 0.3f && pNoise[i][j] <= 0.4f)
                        textureColors[j * height + i] = Color.DodgerBlue;
                    else if (pNoise[i][j] >= 0.43f && pNoise[i][j] <= 0.6f)
                        textureColors[j * height + i] = Color.Black;
                    //textureColors[j * height + i] = Color.LightGreen;
                    else if (pNoise[i][j] >= 0.6f && pNoise[i][j] <= 0.8f)
                        textureColors[j * height + i] = Color.Green;
                    else if (pNoise[i][j] >= 0.8f && pNoise[i][j] <= 1f)
                        textureColors[j * height + i] = Color.DarkGreen;
                    else
                        textureColors[j * height + i] = Color.DodgerBlue;
                }
            }
            p.SetData(textureColors);
            lastMap = p;

            return p;
            p = null;
        }

        public override void LoadContent()
        {
            backgroundColor = Color.FromNonPremultiplied( 50, 50, 50, 255);
            font = ScreenManager.Content.getFont("Express");

            prefab = Content.getTexture("prefab");

            connectionLog = new List<string>();

            maps = new List<Texture2D>();
            mapsData = new List<string>();
            noiseMaps = new List<float[][]>();
            smoothMaps = new List<float[][]>();
            perlinMaps = new List<float[][]>();

            mapsData.Add("s:" + smoothOctave.ToString() + " p:" + perlinOctave.ToString());
            maps.Add(createTexture(512, 512));

            new BreakerUI.Components.Textbox(new Vector2(500, 100), "Test", UIManager,' ',50);
            //new BreakerUI.Components.Button(Vector2.Zero, "test", UIManager);

            /*ScreenManager.threadManager.assignWork(handleSQL);

            string[] d = DLC.Split(';');
            foreach (string ss in d)
            {
                string[] dlcD = ss.Split(',');

                string name = dlcD[0];
                string price = dlcD[1];
                string available = dlcD[2];
                //connectionLog.Add(ss);
            }*/
            /*for (int i = 0; i < 4; i++)
            {
                if (i > 0)
                    circle.Add(new Vector2(circle[i - 1].X + 20, circle[i - 1].Y + 10));
                else
                    circle.Add(new Vector2(20, 0));
            }*/

            base.LoadContent();
        }

        void handleSQL()
        {
            MySql.Data.MySqlClient.MySqlConnection connection;
            string conString = "server=lildolla.xyz;uid=LilDolla;pwd=Be03121997;database=breakerNet";

            try
            {
                connection = new MySql.Data.MySqlClient.MySqlConnection();
                connection.ConnectionString = conString;
                connection.Open();

                try
                {
                    string sql = "SELECT * FROM discoverseData WHERE setting = 'DLC'";
                    MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(sql, connection);
                    MySql.Data.MySqlClient.MySqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        DLC = rdr[2].ToString();
                    }
                    rdr.Close();
                }
                catch (MySql.Data.MySqlClient.MySqlException ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public override void UnloadContent()
        {
            base.UnloadContent();
        }

        public override void Update(GameTime gameTime)
        {
            int prevSize = 2048;
            /*if (input.KeyPressed(Keys.Q))
            {
                if (smoothOctave > 2)
                    smoothOctave -= 1;
            }

            if (input.KeyPressed(Keys.E))
            {
                if (smoothOctave < 16)
                    smoothOctave += 1;
            }

            if (input.KeyPressed(Keys.A))
            {
                if (perlinOctave > 2)
                    perlinOctave -= 1;
            }

            if (input.KeyPressed(Keys.D))
            {
                if (perlinOctave < 16)
                    perlinOctave += 1;
            }*/

            if (input.KeyPressed(Keys.T))
            {
                if (maps2make > 0)
                {
                    maps2make -= 1;
                    maps.Add(createTexture(prevSize, prevSize));
                    mapsData.Add("s:" + smoothOctave.ToString() + " p:" + perlinOctave.ToString());
                }
            }

            if (input.KeyPressed(Keys.S))
            {
                Stream stream = File.Create(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Discoverse\\Maps\\" + DateTime.Now.ToString("MM-dd-yy H;mm;ss") + ".png");
                lastMap.SaveAsPng(stream, prevSize, prevSize);
                stream.Close();

                StreamWriter s = File.CreateText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Discoverse\\Maps\\" + DateTime.Now.ToString("Perlinmap MM-dd-yy H;mm;ss") + ".txt");
                for (int i = 0; i < 2048; i++)
                {
                    for (int j = 0; j < 2048; j++)
                    {
                        float e = perlinMaps[index][i][j];
                        s.WriteLine("[" + i.ToString() + "]" + "[" + j.ToString() + "] = " + e.ToString());
                    }
                }
                s.Close();
            }

            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            ScreenManager.GraphicsDevice.Clear(this.backgroundColor);
            spriteBatch.Begin();

            for (int i = 0; i < connectionLog.Count; i++)
            {
                spriteBatch.DrawString(font, connectionLog[i], new Vector2(10, 24 + (i * 24)), Color.White);
            }

            int si = 512;
            for (int i = 0; i < maps.Count; i++)
            {
                spriteBatch.Draw(maps[i], new Rectangle(0 + (i * (si + 2)), 128, si, si), Color.White);
            }

            spriteBatch.End();

            base.Draw(spriteBatch);
        }

        void DrawLine(SpriteBatch sb, Vector2 start, Vector2 end)
        {
            Vector2 edge = end - start;
            // calculate angle to rotate line
            float angle =
                (float)Math.Atan2(edge.Y, edge.X);


            sb.Draw(Content.getTexture("prefab"),
                new Rectangle(// rectangle defines shape of line and position of start of line
                    (int)start.X,
                    (int)start.Y,
                    (int)edge.Length(), //sb will strech the texture to fill this rectangle
                    1), //width of line, change this to make thicker line
                null,
                Color.Red, //colour of line
                angle,     //angle of line (calulated above)
                new Vector2(0, 0), // point in line about which to rotate
                SpriteEffects.None,
                0);

        }
    }
}
