﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Steamworks;

using Discoverse.Systems;

namespace Discoverse
{
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Texture2D cursor;

        ScreenManager screenManager;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            screenManager = new ScreenManager(this);

            //graphics.IsFullScreen = true;
            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 720;
            graphics.PreferMultiSampling = true;
            graphics.ApplyChanges();

            this.Window.Position = new Point(GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width / 2 - graphics.PreferredBackBufferWidth / 2, GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height / 2 - graphics.PreferredBackBufferHeight / 2);
            this.Window.AllowAltF4 = false;

            /*GameScreen menu = new Discoverse.Screens.GameMenuScreen();
            screenManager.AddScreen(menu);
            screenManager.RemoveScreen(menu);
            GameScreen menu2 = new Discoverse.Screens.LightingScreen();
            screenManager.AddScreen(menu2);*/
#if (!DEBUG)
            GameScreen utility = new Screens.SplashScreen();
#else
            GameScreen utility = new Screens.MenuScreen();
#endif
            screenManager.AddScreen(utility);

            cursor = Content.Load<Texture2D>("Textures/cursor");
        }

        protected override void UnloadContent()
        {
            ScreenManager.sqliteManager.UnloadContent();
        }

        protected override void Update(GameTime gameTime)
        {
            /*if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();*/

            screenManager.Update(gameTime);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.FromNonPremultiplied(0, 102, 153, 255));

            screenManager.Draw(gameTime);
            spriteBatch.Begin();
            spriteBatch.Draw(cursor, GameScreen.mousePosition, Color.White);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
