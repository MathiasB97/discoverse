﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Discoverse.Screens;
using Discoverse.Systems;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Discoverse.Components
{
    public class Components
    {
        //static Dictionary<Emitter, int> emitters;
        static Dictionary<PhysicsGameObject, int> physicsObjects;

        public Components()
        {
            //emitters = new Dictionary<Emitter, int>();
            physicsObjects = new Dictionary<PhysicsGameObject, int>();
        }

        /*public static bool addElement(Emitter element)
        {
            if (!emitters.ContainsKey(element))
            {
                emitters.Add(element, element.id);
                return true;
            }

            return false;
        }*/

        public static bool addElement(PhysicsGameObject element)
        {
            if (!physicsObjects.ContainsKey(element))
            {
                physicsObjects.Add(element, element.id);
                return true;
            }

            return false;
        }

        public void Update(GameTime gameTime)
        {
            /*foreach (KeyValuePair<Emitter, int> e in emitters)
                e.Key.Update(gameTime);*/
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            /*foreach (KeyValuePair<Emitter, int> e in emitters)
                e.Key.Draw(spriteBatch);*/
        }
    }
}
