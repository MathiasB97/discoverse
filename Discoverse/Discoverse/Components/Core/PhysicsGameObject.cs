﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Discoverse.Systems;

using FarseerPhysics;
using FarseerPhysics.Collision;
using FarseerPhysics.Collision.Shapes;
using FarseerPhysics.Common;
using FarseerPhysics.Common.ConvexHull;
using FarseerPhysics.Common.Decomposition;
using FarseerPhysics.Common.PhysicsLogic;
using FarseerPhysics.Common.PolygonManipulation;
using FarseerPhysics.Common.TextureTools;
using FarseerPhysics.Controllers;
using FarseerPhysics.DebugView;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using FarseerPhysics.Dynamics.Joints;
using FarseerPhysics.Factories;
using System.Xml.Serialization;
using System.Diagnostics;
using Discoverse.Components;
using System.Windows.Forms;

using Discoverse.Screens;

namespace Discoverse.Systems
{
    public class PhysicsGameObject : BaseComponent
    {
        public enum ObjectType
        {
            Rectangle,
            Triangle,
            Circle,
            Polygon,
            TexturePoly,
        };
        public int id;
        private World world;
        public Texture2D objectTexture;

        public BreakableBody objectBody;
        public Vector2 origin;
        public Vector2 size;
        public Vertices v;

        public bool poly = false;

        public ObjectType objType;

        public BodyType bodyType;

        public float scale;
        public Color color;
        private bool Atomized = false;
        public float rotation;
        public float strength;

        public List<string> createObjectList;

        public bool atomized
        {
            get { return Atomized; }
        }

        private float alpha = 1.0f;

        public float Alpha
        {
            get { return alpha; }
        }

        public PhysicsGameObject(World wrld, Texture2D texture, Color clr, BodyType bodyType, Vector2 Origin, float Strength, float Scale)
        {
            world = wrld;

            objectTexture = texture;
            origin = Origin;
            scale = Scale;
            strength = Strength;
            color = clr;

            objType = ObjectType.TexturePoly;

            id = generateIdentifier();

            objectBody = createPolygonFromTexture(world, texture, origin, scale, bodyType, Strength);
            this.bodyType = bodyType;

            FarseerLevel.physicsObjects.Add(this.objectBody.MainBody, this);

            setupComponent(this);

            // Debug shit [Console/Save]
            createObjectList = new List<string>();
            createObjectList.Add("");
            createObjectList.Add("Texture2D tex = Content.Load<Texture2D>(" + '"' + texture.Name.ToString() + '"' + ");");
            createObjectList.Add("new PhysicsGameObject(world, tex, Color.FromNonPremultiplied( " + clr.R.ToString() + ", " + clr.G.ToString() + ", " + clr.B.ToString() + ", " + clr.A.ToString() + "), BodyType." + bodyType.ToString() + ", new Vector2( " + Origin.X.ToString() + "f, " + Origin.Y.ToString() + "f), " + strength.ToString() + "f, " + scale.ToString() + "f);");
        }

        public PhysicsGameObject(World wrld, Texture2D texture, Color clr, Vector2 Origin, Vector2 Size, BodyType bodyType, float Strength = 5f, float Scale = 1f)
        {
            world = wrld;

            objectTexture = texture;
            origin = Origin;
            scale = Scale;
            strength = Strength;
            color = clr;

            objType = ObjectType.Rectangle;

            size = Size;

            //float density = (size.X * size.Y) * 1000;
            //float density = 1.5f / (size.y - size.X);
            float density = 1.5f / 25;
            Vertices poly = PolygonTools.CreateRectangle(ConvertUnits.ToSimUnits(size.X * 0.5f), ConvertUnits.ToSimUnits(size.Y * 0.5f));
            List<Vertices> pieces = Triangulate.ConvexPartition(poly, TriangulationAlgorithm.Flipcode);
            BreakableBody body = new BreakableBody(pieces, world, density);
            body.MainBody.Position = ConvertUnits.ToSimUnits(origin.X + (size.X / 2), origin.Y + (size.Y / 2));
            this.bodyType = bodyType;
            if (strength <= 0)
                body.Strength = 50000f;
            else
                body.Strength = Strength;

            body.MainBody.BodyType = bodyType;
            body.MainBody.Rotation = 0f;

            world.AddBreakableBody(body);
            objectBody = body;
            FarseerLevel.physicsObjects.Add(this.objectBody.MainBody, this);

            string id = generateIdentifier().ToString();

            // Debug shit [Console/Save]
            createObjectList = new List<string>();
            createObjectList.Add("");
            string name = "tex" + id;
            createObjectList.Add("Texture2D  " + name + " = ScreenManager.Instance.textureManager.getTextureFromName(" + '"' + "prefab" + '"' + ");");
            createObjectList.Add("new PhysicsGameObject(world, " + name + ", Color.FromNonPremultiplied( " + clr.R.ToString() + ", " + clr.G.ToString() + ", " + clr.B.ToString() + ", " + clr.A.ToString() + "), new Vector2( " + Origin.X.ToString() + "f, " + Origin.Y.ToString() + "f), new Vector2( " + Size.X.ToString() + "f, " + Size.Y.ToString() + "f), BodyType." + bodyType.ToString() + ", " + strength.ToString() + "f, " + scale.ToString() + "f);");
        }

        Object crash()
        {
            return null;
        }

        public PhysicsGameObject(World w, Vertices vertice, Color clr, Vector2 po, BodyType bodyType, float Strength = 5f, float Scale = 1f)
        {
            world = w;
            origin = po;
            color = clr;

            scale = Scale;
            strength = Strength;
            //this.size

            objType = ObjectType.Polygon;
            this.bodyType = bodyType;
            poly = true;

            v = new Vertices();
            foreach (Vector2 p in vertice)
            {
                v.Add(p);
            }
            /*Vertices redone = new Vertices();
            foreach (Vector2 pos in vertice)
            {
                redone.Add(ConvertUnits.ToSimUnits(pos));
            }*/

            List<Vertices> pieces = Triangulate.ConvexPartition(vertice, TriangulationAlgorithm.Earclip);

            /*try
            {
                pieces = Triangulate.ConvexPartition(vertice, TriangulationAlgorithm.Bayazit);
            }
            catch (Exception ex)
            {
                switch (ex.Message.ToString())
                {
                    case "Intersecting Constraints":
                        Notifications.addNotification("", "Uh Oh!", "Polygon is too complicated!", 5);
                        break;
                    default:
                        MessageBox.Show(ex.Message.ToString());
                        break;
                }
            }*/

            float density = 1.5f / (pieces.Count / 2);

            BreakableBody body = new BreakableBody(pieces, w, density);
            body.MainBody.Position = ConvertUnits.ToSimUnits(po / (body.Parts.Count * 100));

            if (Strength <= 0)
                body.Strength = 500f;
            else
                body.Strength = Strength;

            body.MainBody.BodyType = bodyType;
            body.MainBody.Rotation = 0f;

            world.AddBreakableBody(body);
            objectBody = body;
            FarseerLevel.physicsObjects.Add(this.objectBody.MainBody, this);


            // Debug shit [Console/Save]
            string name = "vertice" + generateIdentifier().ToString();

            createObjectList = new List<string>();
            createObjectList.Add("");
            createObjectList.Add("Vertices " + name + " = new Vertices();");
            foreach (Vector2 vt in vertice)
            {
                createObjectList.Add(name + ".Add(new Vector2( " + vt.X.ToString() + "f, " + vt.Y.ToString() + "f));");
            }
            createObjectList.Add("new PhysicsGameObject(world, " + name + ", Color.FromNonPremultiplied( " + clr.R.ToString() + ", " + clr.G.ToString() + ", " + clr.B.ToString() + ", " + clr.A.ToString() + "), new Vector2( " + po.X.ToString() + "f, " + po.Y.ToString() + "f), BodyType." + bodyType.ToString() + ", " + strength.ToString() + "f, " + scale.ToString() + "f);");
        }

        public void Update()
        {
            rotation = objectBody.MainBody.Rotation;
            if (this.objectBody.Broken && Atomized != true)
            {
                Atomized = true;
            }
        }

        public void setRotation(float angle)
        {
            if (!objectBody.Broken)
                objectBody.MainBody.Rotation = angle;
            //objectBody.MainBody.Rotatio
        }

        BreakableBody createPolygonFromTexture(World world, Texture2D texture, Vector2 position, float scale, BodyType bodyType, float strength = 100f)
        {
            uint[] polygonData = new uint[texture.Width * texture.Height];
            texture.GetData(polygonData); // Places all texture data inside polygonData

            // Handle Vertices
            Vertices textureVertices = PolygonTools.CreatePolygon(polygonData, texture.Width, true);
            Vector2 centroid = -textureVertices.GetCentroid();
            textureVertices.Translate(ref centroid);
            origin = -centroid;
            textureVertices = SimplifyTools.ReduceByDistance(textureVertices, 4f);
            List<Vertices> list = Triangulate.ConvexPartition(textureVertices, TriangulationAlgorithm.Bayazit);

            this.scale = scale;

            Vector2 vertScale = new Vector2(ConvertUnits.ToSimUnits(1)) * scale;
            foreach (Vertices vertices in list)
            {
                vertices.Scale(ref vertScale);
            }

            // Create the body
            BreakableBody body = new BreakableBody(list, world, 1);
            body.MainBody.Position = ConvertUnits.ToSimUnits(position.X, position.Y);
            body.MainBody.BodyType = bodyType;
            //if (bodyType == BodyType.Dynamic)
            body.Strength = strength;
            world.AddBreakableBody(body);
            /*Body body = BodyFactory.CreateCompoundPolygon(world, list, 1f, BodyType.Dynamic);
            body.Position = ConvertUnits.ToSimUnits(position.X, position.Y);
            body.BodyType = bodyType;*/


            return body;
        }
    }
}
