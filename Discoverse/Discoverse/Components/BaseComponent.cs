﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

using Discoverse.Systems;
using Discoverse.Screens;


namespace Discoverse.Components
{
    public class BaseComponent
    {
        public ContentManagement content;
        public Random random;

        public BaseComponent()
        {
            random = new Random();
            content = GameScreen.Content;

            Type type = this.GetType();

            /*if (type.ToString() == "Discoverse.Components.Emitter")
                setupComponent((Emitter)Activator.CreateInstance(type));*/
        }

        /*public void setupComponent(Emitter component)
        {
            Components.addElement(component);
        }*/

        public void setupComponent(PhysicsGameObject component)
        {
            Components.addElement(component);
        }

        public int generateIdentifier()
        {
            int i = random.Next(0, 99999);

            return i;
        }

        public virtual void Update(GameTime gameTime)
        {
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
        }
    }
}
