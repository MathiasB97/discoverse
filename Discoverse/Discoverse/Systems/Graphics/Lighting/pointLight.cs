﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discoverse.Systems
{
    public class pointLight : Light
    {
        public pointLight()
            : base(LightType.Point)
        {

        }

        public override Light deepCopy()
        {
            var newLight = new pointLight();
            CopyBaseFields(newLight);

            return newLight;
        }
    }
}
