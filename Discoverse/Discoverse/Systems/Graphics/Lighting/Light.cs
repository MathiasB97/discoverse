﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Discoverse.Systems
{
    public enum LightType
    {
        Point,
        Spot
    }

    public abstract class Light
    {
        protected float initialPower;
        public Vector3 position { get; set; }
        public Vector4 color;

        [ContentSerializerIgnore]
        public float actualPower { get; set; }

        public float power
        {
            get { return initialPower; }
            set
            {
                initialPower = value;
                actualPower = initialPower;
            }
        }

        public float lightDecay { get; set; }

        [ContentSerializerIgnore]
        public LightType lightType { get; private set; }
        [ContentSerializer(Optional = true)]
        public bool isEnabled { get; set; }

        [ContentSerializer(Optional = true)]
        public Vector3 direction { get; set; }

        protected Light(LightType lightType)
        {
            this.lightType = lightType;
        }

        public void enableLight(bool enabled, float timeToEnable)
        {
            isEnabled = enabled;
        }

        public virtual void Update(GameTime gameTime)
        {
            if (!isEnabled) return;
        }

        protected void CopyBaseFields(Light light)
        {
            light.color = this.color;
            light.isEnabled = this.isEnabled;
            light.lightDecay = this.lightDecay;
            light.lightType = this.lightType;
            light.position = this.position;
            light.power = this.power;
        }

        public abstract Light deepCopy();
    }
}
