﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Discoverse.Systems
{
    public class Particle
    {
        public Texture2D texture { get; set; } // Texture of the particle (That'll be drawn)
        public Vector2 position { get; set; } // Position of the particle
        public Vector2 velocity { get; set; } // Speed of the particle
        public float angle { get; set; } // Current angle of the particle
        public float angularVelocity { get; set; } // Speed the angle will change
        public Color color { get; set; } // Color of the particle
        public float size { get; set; } // Size of the particle
        public int life { get; set; } // Life cycle of the particle
        public float alpha = 0; // Alpha of the particle

        public Particle(Texture2D texture, Vector2 position, Vector2 velocity, float angle, float angularVelocity, Color color, float size, int survivalTime)
        {
            this.texture = texture;
            this.position = position;
            this.velocity = velocity;
            this.angle = angle;
            this.angularVelocity = angularVelocity;
            this.color = color;
            this.size = size;
            this.life = survivalTime;
        }

        public void Update()
        {
            life--;
            position += velocity;
            angle += angularVelocity;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            Rectangle sourceRect = new Rectangle(0, 0, texture.Width, texture.Height);
            Vector2 origin = new Vector2(texture.Width / 2, texture.Height / 2);

            /*if (particle.alpha == 0.0f && particles[index].alpha <= 1.0f)
                particle.alpha += 0.1f;*/

            float alfa = life / 100f;

            if (alpha <= 1.0f)
                alpha += 0.01f;

            if (alpha < 1.0f)
                spriteBatch.Draw(texture, position, sourceRect, color * alpha, angle, origin, size, SpriteEffects.None, 0f);
            else
                spriteBatch.Draw(texture, position, sourceRect, color * alfa, angle, origin, size, SpriteEffects.None, 0f);
        }
    }
}
