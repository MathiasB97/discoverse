﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Discoverse.Systems
{
    public class ParticleSystem
    {
        private Random random;
        //private SpriteBatch particleBatch;
        private List<Particle> particles;
        private List<Texture2D> textures;

        public bool active = true;

        public int particleLife = 0;
        public int particleCount { get; set; }
        public Vector2 OriginalPosition { get; private set; }
        public Vector2 EmitterLocation { get; set; }
        public List<Color> colors;
        public float speed { get; set; }

        private Particle generateNewParticle()
        {
            Texture2D texture = textures[random.Next(textures.Count)];
            Vector2 velocity = new Vector2(speed * (float)(random.NextDouble() * 2 - 1), speed * (float)(random.NextDouble() * 2 - 1));
            Vector2 position = EmitterLocation;
            float angle = 0;
            float angularVelocity = 0.1f * (float)(random.NextDouble() * 2 - 1);

            Color color;
            if (this.colors.Count == 0)
                color = new Color((float)random.NextDouble(), (float)random.NextDouble(), (float)random.NextDouble());
            else
                color = this.colors[random.Next(colors.Count)];

            float size = (float)random.NextDouble();

            int tlife;
            if (particleLife == 0)
                tlife = 60 + random.Next(40);
            else
                tlife = particleLife;

            return new Particle(texture, position, velocity, angle, angularVelocity, color, size, tlife);
        }

        public ParticleSystem(List<Texture2D> textures, Vector2 location)
        {
            //particleBatch = new SpriteBatch(ScreenManager.device);

            OriginalPosition = location;
            EmitterLocation = location;
            this.textures = textures;
            this.particles = new List<Particle>();
            random = new Random();
            speed = 1.0f;
            particleCount = 25;

            this.colors = new List<Color>();
        }

        public void Update()
        {
            for (int i = 0; i < particleCount; i++)
            {
                if (active)
                    particles.Add(generateNewParticle());
            }

            for (int particle = 0; particle < particles.Count; particle++)
            {
                particles[particle].Update();
                if (particles[particle].life <= 0)
                {
                    particles.RemoveAt(particle);
                    particle--;
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            //particleBatch.Begin(SpriteSortMode.Immediate, BlendState.Additive);
            for (int index = 0; index < particles.Count; index++)
            {
                particles[index].Draw(spriteBatch);
            }
            //particleBatch.End();
        }
    }
}
