﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Discoverse.Systems
{
    class AParticle
    {
        public Color color;
        public Vector2 position;
        public Vector2 oldPosition;
        public Vector2 velocity;
        public float relativeLength;
        public float relativeLengthSq;
        public float angle;

        public AParticle(Color color, Vector2 position, Vector2 velocity)
        {
            this.color = color;
            this.position = position;
            oldPosition = position;
            this.velocity = velocity;
        }
    }
}
