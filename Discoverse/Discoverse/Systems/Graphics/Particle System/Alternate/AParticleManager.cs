﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace Discoverse.Systems
{
    class AParticleManager
    {
        public const int maxParticles = 25000;
        private int activeParticles;
        private AParticle[] particles;

        public int particleCount
        {
            get { return activeParticles; }
        }

        Texture2D _particle;

        private Vector2 topLeft, bottomRight;

        private Vector2 _leftEdgeNormal = new Vector2(1, 0);
        private Vector2 _rightEdgeNormal = new Vector2(-1, 0);
        private Vector2 _topEdgeNormal = new Vector2(0, 1);
        private Vector2 _bottomEdgeNormal = new Vector2(0, -1);

        Vector2 gravity;

        public AParticleManager()
        {
            Random r = new Random();
            particles = new AParticle[maxParticles];

            gravity = new Vector2(0, 0.15f);

            topLeft = new Vector2(64, 64);
            bottomRight = GameScreen.Dimensions - new Vector2(64, 64);

            for (int i = 0; i < maxParticles; i++)
            {
                particles[i] = new AParticle(new Color(0.25f, 0.25f, 1f) * 0.25f, new Vector2(200f, 200f), new Vector2((float)r.NextDouble(), (float)r.NextDouble()));
            }

            /*_particle = new Texture2D(ScreenManager.device, 1, 1);
            _particle.SetData<Color>(new Color[] { Color.White });*/
            _particle = GameScreen.Content.getTexture("prefab");
        }

        private Vector2 Reflect(ref Vector2 vector, ref Vector2 normal, float restitution)
        {
            return vector - restitution * Vector2.Dot(normal, vector) * normal;
        }

        public void Update(GameTime gameTime)
        {
            int increment = ScreenManager.input.KeyDown(Keys.LeftShift) ? 1000 : 100;

            if (ScreenManager.input.KeyDown(Keys.OemMinus))
                activeParticles = Math.Max(0, activeParticles - increment);

            if (ScreenManager.input.KeyDown(Keys.OemPlus))
                activeParticles = Math.Min(maxParticles - 1, activeParticles + increment);

            for (int i = 0; i < activeParticles; i++)
            {
                AParticle particle = particles[i];
                Vector2 force = Vector2.Zero;

                Vector2 relMouse = GameScreen.mousePosition - particle.position;
                float distanceSq = relMouse.LengthSquared();
                if (Mouse.GetState().LeftButton == ButtonState.Pressed)
                {
                    force = relMouse * (1 / distanceSq) * 100f;
                }

                Vector2 newVelocity = particle.velocity + force + gravity;
                Vector2 newPosition = particle.position + newVelocity;

                // If the new position is crossing a boundary, reflect the velocity against the normal of that edge.
                if (newPosition.X < topLeft.X)
                {
                    newVelocity = Reflect(ref newVelocity, ref _leftEdgeNormal, 1.1f);
                }
                else if (newPosition.X > bottomRight.X)
                {
                    newVelocity = Reflect(ref newVelocity, ref _rightEdgeNormal, 1.1f);
                }
                if (newPosition.Y < topLeft.Y)
                {
                    newVelocity = Reflect(ref newVelocity, ref _topEdgeNormal, 1.1f);
                }
                else if (newPosition.Y > bottomRight.Y)
                {
                    newVelocity = Reflect(ref newVelocity, ref _bottomEdgeNormal, 1.1f);
                }

                particle.oldPosition = particle.position;
                particle.velocity = newVelocity;
                particle.position += particle.velocity;

                particle.position = Vector2.Min(bottomRight, Vector2.Max(topLeft, particles[i].position));

                Vector2 relative = particle.oldPosition - particle.position;
                particle.relativeLengthSq = relative.LengthSquared();
                particle.relativeLength = (float)Math.Sqrt(particle.relativeLengthSq);
                particle.angle = (float)Math.Atan2(relative.Y, relative.X);
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < activeParticles; i++)
            {
                AParticle particle = particles[i];

                float speed = (float)Math.Max(0.1f, (float)Math.Max(particle.relativeLengthSq * 0.05f, 1f));

                spriteBatch.Draw(_particle, particle.position, _particle.Bounds, particle.color * speed, particle.angle, new Vector2(0, 1), new Vector2(particle.relativeLength / 2f, 1f), SpriteEffects.None, 0f);
            }
        }
    }
}
