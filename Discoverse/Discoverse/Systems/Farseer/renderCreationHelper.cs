﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

using FarseerPhysics;
using FarseerPhysics.Collision;
using FarseerPhysics.Collision.Shapes;
using FarseerPhysics.Common;
using FarseerPhysics.Common.ConvexHull;
using FarseerPhysics.Common.Decomposition;
using FarseerPhysics.Common.PhysicsLogic;
using FarseerPhysics.Common.PolygonManipulation;
using FarseerPhysics.Common.TextureTools;
using FarseerPhysics.Controllers;
using FarseerPhysics.DebugView;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using FarseerPhysics.Dynamics.Joints;
using FarseerPhysics.Factories;

namespace Discoverse.Systems.Mechanics
{
    public class renderCreationHelper
    {
        public enum RenderCategory
        {
            Nothing,
            Rectangle,
            Circle,
            Triangle,
            Polygon
        };

        bool rendering = false;
        Texture2D prefab;

        public Vector2 position;
        public Vector2 size;

        bool compatible = false;
        Color compatColor = Color.Red;
        public bool Compatible
        {
            get { return compatible; }
        }

        public RenderCategory renderState;

        //ParticleEngine particles;

        // Polygon related variables
        public Vertices polygonLines;

        public renderCreationHelper()
        {
            // Create the prefabric
            /*prefab = new Texture2D(GameScreen.device, 1, 1);
            prefab.SetData<Color>(new Color[] { Color.White });*/
            //prefab = GameScreen.textureManager.getTextureFromName("prefab");
            prefab = GameScreen.Content.getTexture("prefab");
            //particles = new ParticleEngine(new List<Texture2D>().Add(prefab), new Vector2(position.X, re));

            renderState = RenderCategory.Nothing;
        }

        public void toggleRender(RenderCategory category)
        {
            if (!rendering)
                rendering = true;
            else
                rendering = false;

            renderState = category;
        }

        public void Update()
        {
            if (compatible)
                compatColor = Color.Green;
            else
                compatColor = Color.Red;

            if (rendering == true && renderState != RenderCategory.Nothing)
            {
                // Update rectangle rendering
                if (renderState == RenderCategory.Rectangle)
                {
                    int width = (int)-position.X + (int)GameScreen.mousePosition.X;
                    int height = (int)-position.Y + (int)GameScreen.mousePosition.Y;

                    bool positiveWidth = width > 10;
                    bool positiveHeight = height > 10;

                    bool negativeWidth = width < 0;
                    bool negativeHeight = height < 0;

                    if (positiveWidth == true && positiveHeight == true)
                    {
                        compatible = true;
                    }
                    else if (negativeWidth == true && negativeHeight == true)
                    {
                        compatible = false;
                    }
                    else
                    {
                        compatible = false;
                    }
                }
                // Update polygon rendering
                if (renderState == RenderCategory.Polygon)
                {
                    if (polygonLines.Count >= 1)
                    {
                        Vector2 first = ConvertUnits.ToDisplayUnits(polygonLines[0]);
                        Vector2 last = ConvertUnits.ToDisplayUnits(polygonLines[polygonLines.Count - 1]);
                        float distance = Vector2.Distance(first, last);

                        if (polygonLines.Count > 2 && distance <= 20f)
                        {
                            compatible = true;
                        }
                        else
                        {
                            compatible = false;
                        }
                    }
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            // Render
            int w = 2; // Width of lines
            if (rendering == true && renderState != RenderCategory.Nothing)
            {
                // Render rectangle
                if (renderState == RenderCategory.Rectangle)
                {
                    DrawLine(spriteBatch, new Vector2(position.X, position.Y), new Vector2(GameScreen.mousePosition.X, position.Y), compatColor * 0.5f, w); // Top
                    DrawLine(spriteBatch, new Vector2(position.X, GameScreen.mousePosition.Y), GameScreen.mousePosition, compatColor * 0.5f, w); // Bottom
                    DrawLine(spriteBatch, new Vector2(position.X, position.Y), new Vector2(position.X, GameScreen.mousePosition.Y), compatColor * 0.5f, w); // Left
                    DrawLine(spriteBatch, new Vector2(GameScreen.mousePosition.X, position.Y), GameScreen.mousePosition, compatColor * 0.5f, w); // Right
                    DrawLine(spriteBatch, new Vector2(position.X, GameScreen.mousePosition.Y), new Vector2(GameScreen.mousePosition.X, position.Y), compatColor * 0.5f, w); // Diagonal
                }
                // Render polygon lines
                if (renderState == RenderCategory.Polygon)
                {
                    for (int i = 0; i < polygonLines.Count; i++)
                    {
                        if (i >= 1)
                        {
                            Vector2 lastLine = ConvertUnits.ToDisplayUnits(polygonLines[(i - 1)]);
                            Vector2 nextLine = ConvertUnits.ToDisplayUnits(polygonLines[i]);
                            DrawLine(spriteBatch, lastLine, nextLine, compatColor * 0.5f, w);
                        }
                    }
                }
            }
        }

        void DrawLine(SpriteBatch spriteBatch, Vector2 begin, Vector2 end, Color color, int width = 1)
        {
            Rectangle r = new Rectangle((int)begin.X, (int)begin.Y, (int)(end - begin).Length() + width, width);
            Vector2 v = Vector2.Normalize(begin - end);
            float angle = (float)Math.Acos(Vector2.Dot(v, -Vector2.UnitX));
            if (begin.Y > end.Y) angle = MathHelper.TwoPi - angle;
            spriteBatch.Draw(prefab, r, null, color, angle, Vector2.Zero, SpriteEffects.None, 0);
        }
    }
}
