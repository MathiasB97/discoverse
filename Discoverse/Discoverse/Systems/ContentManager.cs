﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Discoverse.Systems
{
    public class ContentManagement
    {
        private int texCount = 0;
        private int fontCount = 0;
        private int effectCount = 0;
        private int modelCount = 0;

        private Dictionary<string, Texture2D> textures;
        private Dictionary<string, SpriteFont> fonts;
        private Dictionary<string, Effect> effects;
        private Dictionary<string, Model> models;

        private GraphicsDevice device;
        private ContentManager content;

        public ContentManagement(ContentManager manager, GraphicsDevice device)
        {
            this.content = manager;
            this.device = device;

            this.textures = new Dictionary<string, Texture2D>();
            this.fonts = new Dictionary<string, SpriteFont>();
            this.effects = new Dictionary<string, Effect>();
            this.models = new Dictionary<string, Model>();

            Texture2D tex = new Texture2D(device, 1, 1, false, SurfaceFormat.Color);
            tex.SetData<Color>(new Color[] { Color.White });
            addTexture("prefab", tex);
        }

        public ContentManager ContentManager()
        {
            return content;
        }

        // Methods of getting content
        #region Textures
        public Texture2D addTexture(string name, string path)
        {
            if (!textures.ContainsKey(name.ToLower()))
            {
                Texture2D texture = content.Load<Texture2D>(path);
                textures.Add(name.ToLower(), texture);
                texCount += 1;
                Console.WriteLine("[Texture][ContentManagement] Added '" + name.ToLower() + "' to cache.");

                return texture;
            }
            else
            {
                return getTexture(name);
            }
        }

        public Texture2D addTexture(string name, Texture2D texture)
        {
            if (!textures.ContainsKey(name.ToLower()))
            {
                textures.Add(name.ToLower(), texture);
                texCount += 1;
                Console.WriteLine("[Texture][ContentManagement] Added '" + name.ToLower() + "' to cache.");

                return texture;
            }
            else
            {
                return getTexture(name);
            }
        }

        public Texture2D getTexture(string texName)
        {
            Texture2D tex = new Texture2D(device, 1, 1, false, SurfaceFormat.Color);
            tex.SetData<Color>(new Color[] { Color.White });

            for (int i = 0; i < texCount; i++)
            {
                if (textures.ContainsKey(texName.ToLower()))
                {
                    tex = null;

                    tex = textures[texName.ToLower()];
                }
                else
                    tex = getTexture("prefab");
            }

            return tex;
        }
        #endregion
        #region Fonts
        public SpriteFont addFont(string name, string path)
        {
            if (!fonts.ContainsKey(name.ToLower()))
            {
                SpriteFont font = content.Load<SpriteFont>(path);
                fonts.Add(name.ToLower(), font);
                fontCount += 1;
                Console.WriteLine("[Font][ContentManagement] Added '" + name.ToLower() + "' to cache.");

                return font;
            }
            else
            {
                return getFont(name);
            }
        }

        public SpriteFont getFont(string fontName)
        {
            SpriteFont font = null;
            if (fonts.ContainsKey(fontName.ToLower()))
                font = fonts[fontName.ToLower()];
            else
                font = getFont("Express");
            /*for (int i = 0; i < fontCount; i++)
            {
                if (fonts.ContainsKey(fontName.ToLower()))
                {
                    font = fonts[fontName.ToLower()];
                }
                else
                    font = getFont("Expressway");
            }*/
            return font;
        }
        #endregion
        #region Effects
        public Effect addEffect(string name, string path)
        {
            if (!effects.ContainsKey(name.ToLower()))
            {
                Effect effect = content.Load<Effect>(path);
                effects.Add(name.ToLower(), effect);
                effectCount += 1;
                Console.WriteLine("[Effect][ContentManagement] Added '" + name.ToLower() + "' to cache.");

                return effect;
            }
            else
            {
                return getEffect(name);
            }
        }

        public Effect getEffect(string effectName)
        {
            Effect effect = null;
            for (int i = 0; i < effectCount; i++)
            {
                if (effects.ContainsKey(effectName.ToLower()))
                {
                    effect = effects[effectName.ToLower()];
                }
                else
                    effect = getEffect("Expressway");
            }
            return effect;
        }
        #endregion
        #region Models
        public Model addModel(string name, string path)
        {
            if (!models.ContainsKey(name.ToLower()))
            {
                Model model = content.Load<Model>(path);
                models.Add(name.ToLower(), model);
                modelCount += 1;
                Console.WriteLine("[Model][ContentManagement] Added '" + name.ToLower() + "' to cache.");

                return model;
            }
            else
            {
                return getModel(name);
            }
        }

        public Model getModel(string name)
        {
            Model model = null;
            for (int i = 0; i < modelCount; i++)
            {
                if (models.ContainsKey(name.ToLower()))
                {
                    model = models[name.ToLower()];
                }
            }
            return model;
        }
        #endregion
    }
}
