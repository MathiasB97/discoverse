﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Diagnostics;
using Microsoft.Xna.Framework.Input;

namespace Discoverse.Systems
{
    public enum ScreenState
    {
        TransitionOn,
        Active,
        TransitionOff,
        Hidden,
    }

    public class GameScreen
    {
        private string scName;
        ScreenState screenState = ScreenState.TransitionOn;
        static ScreenManager screenManager;
        public InputManager input;
        //public Keybinding keyBinds;
        static Vector2 cursorPosition;
        public static Vector2 Dimensions;
        public bool active = true;
        public bool popup = false;
        public Color backgroundColor = Color.FromNonPremultiplied(51, 102, 0, 255);
        public PerformanceCounter ramCounter;

        public NotificationManager Notifications
        {
            get { return ScreenManager.notifications; }
        }

        public string screenName
        {
            get { return scName; }
        }

        public SQLiteManager sqliteManager
        {
            get { return ScreenManager.sqliteManager; }
        }

        public ScreenState ScreenState
        {
            get { return screenState; }
            protected set { screenState = value; }
        }

        public ScreenManager ScreenManager
        {
            get { return screenManager; }
            internal set { screenManager = value; }
        }

        public BreakerUI.Manager UIManager
        {
            get { return ScreenManager.ui_manager; }
        }

        public static ContentManagement Content
        {
            get { return screenManager.Content; }
        }

        public static GraphicsDevice device
        {
            get { return ScreenManager.device; }
        }

        public static Vector2 mousePosition
        {
            get { return cursorPosition; }
        }

        #region Properties
        public bool IsExiting
        {
            get { return isExiting; }
            protected set { isExiting = value; }
        }
        bool isExiting = false;

        public bool IsActive
        {
            get
            {
                return !otherScreenHasFocus &&
                       (screenState == ScreenState.TransitionOn ||
                        screenState == ScreenState.Active);
            }
        }
        bool otherScreenHasFocus;

        public TimeSpan TransitionOnTime
        {
            get { return transitionOnTime; }
            protected set { transitionOnTime = value; }
        }

        TimeSpan transitionOnTime = TimeSpan.Zero;


        /// <summary>
        /// Indicates how long the screen takes to
        /// transition off when it is deactivated.
        /// </summary>
        public TimeSpan TransitionOffTime
        {
            get { return transitionOffTime; }
            protected set { transitionOffTime = value; }
        }

        TimeSpan transitionOffTime = TimeSpan.Zero;


        /// <summary>
        /// Gets the current position of the screen transition, ranging
        /// from zero (fully active, no transition) to one (transitioned
        /// fully off to nothing).
        /// </summary>
        public float TransitionPosition
        {
            get { return transitionPosition; }
            protected set { transitionPosition = value; }
        }

        float transitionPosition = 1;


        /// <summary>
        /// Gets the current alpha of the screen transition, ranging
        /// from 255 (fully active, no transition) to 0 (transitioned
        /// fully off to nothing).
        /// </summary>
        public byte TransitionAlpha
        {
            get { return (byte)(255 - TransitionPosition * 255); }
        }
        #endregion;

        public virtual void Init()
        {
            scName = this.GetType().Name.ToString();
        }

        public virtual void LoadContent()
        {
            input = ScreenManager.input;
            //keyBinds = screenManager.keyBinds;

            ramCounter = new PerformanceCounter("Memory", "Available MBytes");
            Dimensions = new Vector2(1280, 720);
        }

        public virtual void UnloadContent()
        {

        }

        public virtual void Update(GameTime gameTime)
        {
            this.otherScreenHasFocus = otherScreenHasFocus;

            if (Mouse.GetState().Position.ToVector2().X > 0 && Mouse.GetState().Position.ToVector2().Y > 0)
                if (Mouse.GetState().Position.ToVector2().X < Dimensions.X && Mouse.GetState().Position.ToVector2().Y < Dimensions.Y)
                    cursorPosition = Mouse.GetState().Position.ToVector2();

            if (isExiting)
            {
                // If the screen is going away to die, it should transition off.
                screenState = ScreenState.TransitionOff;

                if (!UpdateTransition(gameTime, transitionOffTime, 1))
                {
                    // When the transition finishes, remove the screen.
                    ScreenManager.RemoveScreen(this);

                    isExiting = false;
                }
            }
            /*else if (coveredByOtherScreen)
            {
                // If the screen is covered by another, it should transition off.
                if (UpdateTransition(gameTime, transitionOffTime, 1))
                {
                    // Still busy transitioning.
                    screenState = ScreenState.TransitionOff;
                }
                else
                {
                    // Transition finished!
                    screenState = ScreenState.Hidden;
                }
            }*/
            else
            {
                // Otherwise the screen should transition on and become active.
                if (UpdateTransition(gameTime, transitionOnTime, -1))
                {
                    // Still busy transitioning.
                    screenState = ScreenState.TransitionOn;
                }
                else
                {
                    // Transition finished!
                    screenState = ScreenState.Active;
                }
            }
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            //ScreenManager.GraphicsDevice.Clear(backgroundColor);
/*#if (DEBUG)
            spriteBatch.Begin();
            spriteBatch.Draw(Content.getTexture("prefab"), new Rectangle(0, 0, 1280, 20), Color.White * 0.45f);
            long mem = GC.GetTotalMemory(true);
            mem = mem / 1024 / 1024;

            float gb = ramCounter.NextValue();
            float gb1 = gb / 1024;

            spriteBatch.Draw(Content.getTexture("prefab"), new Rectangle(1280 - 100, 0, 100, 20), Color.Black * 0.75f);
            spriteBatch.Draw(Content.getTexture("prefab"), new Rectangle(1280 - 100, 0, (int) (gb - mem) / 100, 20), Color.Red * 0.75f);

            spriteBatch.DrawString(Content.getFont("Express"), ((gb - mem) / 100).ToString() + "%", new Vector2(1280 - Content.getFont("Express").MeasureString((gb - mem).ToString() + "% ").X, 0), Color.Black);

            spriteBatch.End();
#endif*/
        }

        bool UpdateTransition(GameTime gameTime, TimeSpan time, int direction)
        {
            // How much should we move by?
            float transitionDelta;

            if (time == TimeSpan.Zero)
                transitionDelta = 1;
            else
                transitionDelta = (float)(gameTime.ElapsedGameTime.TotalMilliseconds /
                                          time.TotalMilliseconds);

            // Update the transition position.
            transitionPosition += transitionDelta * direction;

            // Did we reach the end of the transition?
            if ((transitionPosition <= 0) || (transitionPosition >= 1))
            {
                transitionPosition = MathHelper.Clamp(transitionPosition, 0, 1);
                return false;
            }

            // Otherwise we are still busy transitioning.
            return true;
        }
    }
}
