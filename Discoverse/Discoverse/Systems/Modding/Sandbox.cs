﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using MoonSharp.Interpreter;
using BreakerUI;
using Discoverse.Systems;
using BreakerUI.Components;
using System.Reflection;
using System.IO;

namespace Discoverse.Systems.Modding
{
    public class Sandbox
    {
        [Flags]
        public enum CoreModules
        {
            None = 0,
            Basic = 0x40,
            GlobalConsts = 0x1,
            TableIterators = 0x2,
            Metatables = 0x4,
            String = 0x8,
            LoadMethods = 0x10,
            Table = 0x20,
            ErrorHandling = 0x80,
            Math = 0x100,
            Coroutine = 0x200,
            Bit32 = 0x400,
            OS_Time = 0x800,
            OS_System = 0x1000,
            IO = 0x2000,
            Debug = 0x4000,
            Dynamic = 0x8000,
            Preset_HardSandbox = GlobalConsts | TableIterators | String | Table | Basic | Math | Bit32,
            Preset_SoftSandbox = Preset_HardSandbox | Metatables | ErrorHandling | Coroutine | OS_Time | Dynamic,
            Preset_Default = Preset_SoftSandbox | LoadMethods | OS_System | IO,
            Preset_Complete = Preset_Default | Debug,

        }

        Dictionary<string, Script> scripts; // All scripts that are loaded into memory;
        private Script mainScript;

        private Manager ui_manager;
        private GraphicsDevice dev;

        public Script Script
        {
            get { return mainScript; }
        }

        // Graphic Inclusion
        Dictionary<string, Texture2D> loadedTextures;
        Dictionary<string, Texture2D> texturesToDraw;
        Dictionary<string, Vector2> stringsToDraw;

        public Sandbox(Manager uiManager, GraphicsDevice device)
        {
            scripts = new Dictionary<string, Script>();
            ui_manager = uiManager;
            dev = device;

            loadedTextures = new Dictionary<string, Texture2D>();
            texturesToDraw = new Dictionary<string, Texture2D>();
            stringsToDraw = new Dictionary<string, Vector2>();

            //outputConsole("Initialized scripting sandbox.");
            outputConsole("Found Scripts: ");
            Dictionary<string, string> foundFiles = new Dictionary<string, string>();
            foreach (var file in Directory.GetFiles(Assembly.GetExecutingAssembly().Location.Replace("\\Discoverse.exe", "") + "\\Scripts\\"))
            {
                if (System.IO.Path.GetExtension(file) == ".lua")
                {
                    string[] f = file.Split('\\');
                    string name = f[f.Length - 1];

                    Script script = new Script();
                    if (script.DoFile(file) != null)
                    {
                        DynValue modInit = script.Call(script.Globals["Initialize"]);
                        if (modInit.Table["title"] != null)
                        {
                            if (modInit.Table["loadScript"].ToString() != "false")
                            {
                                outputConsole("     - " + modInit.Table["title"] + " [" + name + "]");
                                foundFiles.Add(modInit.Table["title"].ToString(), file);
                                script = null;
                            } else
                            {
                                outputConsole("     (Skipped) - " + modInit.Table["title"] + " [" + name + "]");
                                script = null;
                            }
                        }
                    }
                }
            }
            // Load each file that were found
            foreach (KeyValuePair<string, string> file in foundFiles)
            {
                loadScript(file.Value);
                runScript(file.Key);
            }
            foundFiles = null;

            mainScript = new Script();
            // Add functions
            mainScript.Globals["outputConsole"] = (Func<string, bool>)outputConsole;
            mainScript.Globals["createButton"] = (Func<int, int, string, Button>)createButton;
        }

        public bool loadScript(string file)
        {
            Script script = new Script();
            if (script.DoFile(file) != null)
            {
                DynValue modInit = script.Call(script.Globals["Initialize"]);
                if (modInit.Table["title"] != null)
                {
                    outputConsole("Loaded script '" + modInit.Table["title"] + "'.");

                    // Add our functions to the file
                    script.Globals["outputConsole"] = (Func<string, bool>)outputConsole;
                    script.Globals["createButton"] = (Func<int, int, string, Button>)createButton;
                    script.Globals["loadTexture"] = (Func<string, string, int, int, string>)loadTexture;
                    script.Globals["loadTextureWH"] = (Func<string, string, int, int, int, int, string>)loadTexture;
                    script.Globals["drawTexture"] = (Func<string, bool>)drawTexture;
                    script.Globals["drawString"] = (Func<string, int, int, bool>)drawString;

                    scripts.Add(modInit.Table["title"].ToString(), script);
                    return true;
                } else
                {
                    outputConsole("Failed to load script '" + file + "'. Script missing proper initialization.");
                    return false;
                }
            }
            else
            {
                outputConsole("Failed to load script '" + file + "'. Script does not exist.");
                return false;
            }
        }

        public bool runScript(string name)
        {
            if (scripts.ContainsKey(name))
            {
                outputConsole("Running script '" + name + "'.");
                scripts[name].Call(scripts[name].Globals["Main"]);
                return true;
            }
            else
                return false;
        }

        public void stopAllScripts()
        {
            loadedTextures.Clear();
            texturesToDraw.Clear();
            stringsToDraw.Clear();
            foreach (KeyValuePair<string, Script> sD in scripts)
            {
                Script script = sD.Value;
                script = null;
            }
        }

        public void Update()
        {

        }

        public void Draw(SpriteBatch spriteBatch, SpriteFont font)
        {
            foreach (KeyValuePair<string, Texture2D> data in texturesToDraw)
            {
                string[] dName = data.Key.Split(';');
                int x = Convert.ToInt32(dName[1]);
                int y = Convert.ToInt32(dName[2]);

                if ((dName.Length-1) == 2)
                    spriteBatch.Draw(data.Value, new Vector2(x, y), Color.White);
                else
                {
                    int w = Convert.ToInt32(dName[3]);
                    int h = Convert.ToInt32(dName[4]);

                    spriteBatch.Draw(data.Value, new Rectangle(x, y, w, h), Color.White);
                }
            }

            foreach (KeyValuePair<string, Vector2> data in stringsToDraw)
            {
                spriteBatch.DrawString(font, data.Key, data.Value, Color.Red);
            }
        }

        #region Functions

        bool outputConsole(string sentence)
        {
            Console.WriteLine(sentence);
            return true;
        }

        Button createButton(int x, int y, string name)
        {
            Button btn = new Button(new Vector2(x, y), name, ui_manager);

            return btn;
        }

        // Content Management
        string loadTexture(string name, string path, int x, int y)
        {
            Texture2D tex = null;

            string dir = Assembly.GetExecutingAssembly().Location.Replace("\\Discoverse.exe", "") + "\\Scripts\\";
            if (File.Exists(dir + path))
            {
                string loc = dir + path;
                FileStream file = new FileStream(loc, FileMode.Open);

                tex = Texture2D.FromStream(dev, file);
                loadedTextures.Add(name + ";" + x + ";" + y, tex);
                //texturesToDraw.Add(tex);

                file.Close();
            }

            return name + ";" + x + ";" + y;
        }

        string loadTexture(string name, string path, int x, int y, int w, int h)
        {
            Texture2D tex = null;

            string dir = Assembly.GetExecutingAssembly().Location.Replace("\\Discoverse.exe", "") + "\\Scripts\\";
            if (File.Exists(dir + path))
            {
                string loc = dir + path;
                FileStream file = new FileStream(loc, FileMode.Open);

                tex = Texture2D.FromStream(dev, file);
                loadedTextures.Add(name + ";" + x + ";" + y + ";" + w + ";" + h, tex);
                //texturesToDraw.Add(tex);

                file.Close();
            }

            return name + ";" + x + ";" + y + ";" + w + ";" + h;
        }

        bool drawString(string text, int x, int y)
        {
            if (!stringsToDraw.ContainsKey(text))
                stringsToDraw.Add(text, new Vector2(x, y));

            return true;
        }

        bool drawTexture(string dName)
        {
            if (loadedTextures.ContainsKey(dName))
                if (!texturesToDraw.ContainsKey(dName))
                    texturesToDraw.Add(dName, loadedTextures[dName]);

            return true;
        }

        #endregion
    }
}
