﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Discoverse.Systems
{
    // Multi-threading manager class.
    public class ThreadManagement
    {
        public List<Thread> threads;

        Thread mainThread;

        bool working;
        Thread workerThread;

        public ThreadManagement()
        {
            mainThread = Thread.CurrentThread;
            mainThread.Name = "mainThread";

            threads = new List<Thread>();
            threads.Add(mainThread);
            working = false;
        }

        public Thread assignWork(ThreadStart methodToWorkOn)
        {
            if (!working)
            {
                try
                {
                    workerThread = new Thread(methodToWorkOn);
                    workerThread.Name = "workerThread";
                    workerThread.IsBackground = true;
                    working = true;
                    workerThread.Start();
                    workerThread.Join();
                    threads.Add(workerThread);
                    working = false;
                } catch (Exception ex)
                {
                    Console.WriteLine("[ThreadManagement] An error occured. Unable to assign work to workerThread.");
                }
            } else
            {
                Console.WriteLine("[ThreadManagement] Thread is currently busy.");
            }
            return workerThread;
        }
    }
}
