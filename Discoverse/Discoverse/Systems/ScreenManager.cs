﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Steamworks;

using Discoverse.Systems.Modding;
using BreakerNet;
using BreakerUI;

namespace Discoverse.Systems
{
    public class ScreenManager : DrawableGameComponent
    {
        public static GraphicsDeviceManager graphicsDevice;
        public static GraphicsDevice device;
        SpriteBatch spriteBatch;
        ContentManager content;
        public Sandbox moddingSandbox;
        public BreakerNet.BreakerNet breakerNet;
        public ContentManagement Content;
        public static InputManager input;
        public static Keybinding keyBinds;
        List<GameScreen> screens;
        List<GameScreen> updatingScreens;
        List<GameScreen> drawnScreens;
        IGraphicsDeviceService graphicsDeviceService;
        public Manager ui_manager;
        public ThreadManagement threadManager;
        public static NotificationManager notifications;
        public static AchievementManager achievements;
        public static SQLiteManager sqliteManager;

        public SpriteBatch SpriteBatch
        {
            get { return spriteBatch; }
        }

        new public Game Game
        {
            get { return base.Game; }
        }

        public ScreenManager(Game game)
            :base(game)
        {
            threadManager = new ThreadManagement();
            sqliteManager = new SQLiteManager();
            breakerNet = new BreakerNet.BreakerNet("http://lildolla.xyz/myweb");

            content = new ContentManager(game.Services, "Content");
            Content = new ContentManagement(content, GraphicsDevice);
            graphicsDeviceService = (IGraphicsDeviceService)game.Services.GetService(typeof(IGraphicsDeviceService));
            if (graphicsDeviceService == null)
                throw new InvalidOperationException("No graphics device service.");
            device = game.GraphicsDevice;

            spriteBatch = new SpriteBatch(GraphicsDevice);

            screens = new List<GameScreen>();
            updatingScreens = new List<GameScreen>();
            drawnScreens = new List<GameScreen>();

            Content.addTexture("cursor", "Textures/cursor");
            Content.addTexture("GUI", "Textures/GUI");
            Content.addTexture("logo", "Textures/logo");
            Content.addTexture("lock", "Textures/icons/lock");

            Content.addFont("Express", "Fonts/Express");
            Content.addFont("Express2X", "Fonts/Express2X");
            Content.addFont("Roboto", "Fonts/Roboto");
            Content.addFont("Roboto2X", "Fonts/Roboto2X");
            Content.addFont("RobotoLight", "Fonts/RobotoLight");
            Content.addFont("RobotoThin", "Fonts/RobotoThin");
            Content.addFont("Terminal", "Fonts/Terminal");

            Content.addEffect("MultiTarget", "Effects/Lighting/MultiTarget");
            Content.addEffect("DeferredCombined", "Effects/Lighting/DeferredCombined");

            ui_manager = new Manager(content, Content.getTexture("GUI"), GraphicsDevice);
            moddingSandbox = new Sandbox(ui_manager, GraphicsDevice);
            input = new InputManager();
            keyBinds = new Keybinding();
            notifications = new NotificationManager(Content);
            achievements = new AchievementManager(Content);
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            foreach (GameScreen screen in screens)
            {
                screen.Init();
                screen.LoadContent();
            }
        }

        protected override void UnloadContent()
        {
            content.Unload();
            foreach (GameScreen screen in screens)
                screen.UnloadContent();

            sqliteManager.UnloadContent();

            base.UnloadContent();
        }

        public override void Update(GameTime gameTime)
        {
            foreach (GameScreen screen in screens)
                updatingScreens.Add(screen);

            bool otherScreenHasFocus = !Game.IsActive;
            bool coveredByOtherScreen = false;

            while (updatingScreens.Count > 0)
            {
                GameScreen screen = updatingScreens[updatingScreens.Count - 1];
                updatingScreens.RemoveAt(updatingScreens.Count - 1);

                input.Update();
                screen.Update(gameTime);
                notifications.Update(gameTime);
                achievements.Update(gameTime);
                if (screen.ScreenState == ScreenState.TransitionOn ||
                    screen.ScreenState == ScreenState.Active)
                {
                    // If this is the first active screen we came across,
                    // give it a chance to handle input and update presence.
                    if (!otherScreenHasFocus)
                        otherScreenHasFocus = true;

                    // If this is an active non-popup, inform any subsequent
                    // screens that they are covered by it.
                    if (!screen.popup)
                        coveredByOtherScreen = true;
                }
            }

            moddingSandbox.Update();
            ui_manager.Update();

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            drawnScreens.Clear();
            foreach (GameScreen screen in screens)
                drawnScreens.Add(screen);

            foreach (GameScreen screen in drawnScreens)
            {
                if (screen.ScreenState == ScreenState.Hidden)
                    continue;

                screen.Draw(spriteBatch);
            }

            spriteBatch.Begin();
            moddingSandbox.Draw(spriteBatch, Content.getFont("Express"));
            ui_manager.Draw(spriteBatch);
            notifications.Draw(spriteBatch);
            achievements.Draw(spriteBatch);
            spriteBatch.End();

            base.Draw(gameTime);
        }

        #region Public Methods


        /// <summary>
        /// Adds a new screen to the screen manager.
        /// </summary>
        public void AddScreen(GameScreen screen)
        {
            screen.ScreenManager = this;

            // If we have a graphics device, tell the screen to load content.
            if ((graphicsDeviceService != null) &&
                (graphicsDeviceService.GraphicsDevice != null))
            {
                screen.Init();
                screen.LoadContent();
            }

            screens.Add(screen);
        }


        /// <summary>
        /// Removes a screen from the screen manager. You should normally
        /// use GameScreen.ExitScreen instead of calling this directly, so
        /// the screen can gradually transition off rather than just being
        /// instantly removed.
        /// </summary>
        public void RemoveScreen(GameScreen screen)
        {
            // If we have a graphics device, tell the screen to unload content.
            if ((graphicsDeviceService != null) &&
                (graphicsDeviceService.GraphicsDevice != null))
            {
                screen.UnloadContent();
            }

            screens.Remove(screen);
            updatingScreens.Remove(screen);
        }


        /// <summary>
        /// Expose an array holding all the screens. We return a copy rather
        /// than the real master list, because screens should only ever be added
        /// or removed using the AddScreen and RemoveScreen methods.
        /// </summary>
        public GameScreen[] GetScreens()
        {
            return screens.ToArray();
        }
        #endregion
    }
}