﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discoverse.Systems
{
    public enum MappedActions
    {
        Pause,
        Jump,
        Up,
        Down,
        Left,
        Right,
        LeftClick,
        RightClick,
        MiddleClick,
    }

    public enum Playstation_Keys
    {
        Cross,
        Circle,
        Square,
        Triangle,
        L1,
        R1,
        L2,
        R2,
        L3,
        R3,
        DPad,
        Select,
        Start
    }

    public enum Xbox_Keys
    {
        A,
        B,
        X,
        Y,
        LB,
        RB,
        LT,
        RT,
        DPad,
        View,
        Menu
    }

    public enum Gamepad_Type
    {
        PS3,
        Xbox,
    }

    public class Keybinding
    {
        private Dictionary<string, Keys> kbBinds;

        private Dictionary<string, Buttons> xboxBinds;
        private Dictionary<string, Buttons> ps4Binds;

        public List<string> allBinds;
        public Dictionary<MappedActions, ButtonState> buttonMappings;
        public Dictionary<MappedActions, Keys> keyMappings;

        public Keybinding()
        {
            kbBinds = new Dictionary<string, Keys>();
            xboxBinds = new Dictionary<string, Buttons>();
            ps4Binds = new Dictionary<string, Buttons>();

            buttonMappings = new Dictionary<MappedActions, ButtonState>();
            keyMappings = new Dictionary<MappedActions, Keys>();
            allBinds = new List<string>();

            buttonMappings.Add(MappedActions.LeftClick, Mouse.GetState().LeftButton);
            buttonMappings.Add(MappedActions.RightClick, Mouse.GetState().RightButton);
            buttonMappings.Add(MappedActions.MiddleClick, Mouse.GetState().MiddleButton);

            keyMappings[MappedActions.Jump] = Keys.Space;
            keyMappings[MappedActions.Up] = Keys.W;
            keyMappings[MappedActions.Down] = Keys.S;
            keyMappings[MappedActions.Left] = Keys.A;
            keyMappings[MappedActions.Right] = Keys.D;

            /* Default Keyboard Bindings
            addKeybind( "Move Up", Keys.W);
            addKeybind( "Move Left", Keys.A);
            addKeybind( "Move Down", Keys.S);
            addKeybind( "Move Right", Keys.D);
            addKeybind( "Pause", Keys.Escape);*/

            // Default Gamepad Bindings
            addGamepadBind(Gamepad_Type.Xbox, "Move Up", Buttons.DPadUp);
            addGamepadBind(Gamepad_Type.Xbox, "Move Down", Buttons.DPadDown);
            addGamepadBind(Gamepad_Type.Xbox, "Move Left", Buttons.DPadLeft);
            addGamepadBind(Gamepad_Type.Xbox, "Move Right", Buttons.DPadRight);
        }

        public void addGamepadBind(Gamepad_Type type, string bindName, Buttons btn)
        {
            if (type == Gamepad_Type.Xbox)
            {
                if (!xboxBinds.ContainsKey(bindName))
                {
                    xboxBinds.Add(bindName, btn);
                    allBinds.Add(bindName + ";" + btn.ToString());
                }
            }
        }

        public void addKeybind(string bindName, Keys key)
        {
            if (!kbBinds.ContainsKey(bindName))
            {
                kbBinds.Add(bindName, key);
                allBinds.Add(bindName + ";" + key.ToString());
            }
        }

        public void changeKeybind(string bindName, Keys newKey)
        {
            if (kbBinds.ContainsKey(bindName))
            {
                for (int i = 0; i < allBinds.Count; i++)
                {
                    if (allBinds.ElementAt(i) == bindName)
                    {
                        allBinds.RemoveAt(i);
                    }
                }
                //allBinds.Remove(bindName + ";" + allBinds[bindName]);
                kbBinds[bindName] = newKey;
            }
        }

        public Keys getKeybind(string bindName)
        {
            return kbBinds[bindName];
        }

        public Buttons getGamepadBind(Gamepad_Type type, string bindName)
        {
            if (type == Gamepad_Type.Xbox)
                return xboxBinds[bindName];
            else if (type == Gamepad_Type.PS3)
                return ps4Binds[bindName];
            else
                return xboxBinds[bindName];
        }
    }
}
