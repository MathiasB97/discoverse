﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discoverse.Systems
{
    public class InputManager
    {
        public MouseState currentMouseState, prevMouseState;
        KeyboardState currentKeyState, prevKeyState;
        GamePadState currentGamePadState, prevGamePadState;

        bool gamepadConnected = false;

        public bool controlledConnected
        {
            get { return gamepadConnected; }
        }

        private static InputManager instance;
        public static InputManager Instance
        {
            get
            {
                if (instance == null)
                    instance = new InputManager();
                return instance;
            }
        }

        public void Update()
        {
            prevMouseState = currentMouseState;
            prevKeyState = currentKeyState;

            currentMouseState = Mouse.GetState();
            currentKeyState = Keyboard.GetState();

            if (GamePad.GetState(Microsoft.Xna.Framework.PlayerIndex.One).IsConnected)
            {
                prevGamePadState = currentGamePadState;
                if (gamepadConnected != true)
                    gamepadConnected = true;
                currentGamePadState = GamePad.GetState(Microsoft.Xna.Framework.PlayerIndex.One);
            }
        }

        public bool GamepadKeyPressed(Buttons button)
        {
            bool state = false;
            GamePadButtons btn = currentGamePadState.Buttons;

            if (gamepadConnected)
            {
                if (currentGamePadState.IsButtonDown(button) && prevGamePadState.IsButtonUp(button))
                    state = true;
            }

            return state;
        }

        public bool MouseKeyPressed(ButtonState mouseButton)
        {
            bool state = false;

            if (mouseButton == currentMouseState.LeftButton)
                if (mouseButton == ButtonState.Pressed && prevMouseState.LeftButton == ButtonState.Released)
                    state = true;

            if (mouseButton == currentMouseState.MiddleButton)
                if (mouseButton == ButtonState.Pressed && prevMouseState.MiddleButton == ButtonState.Released)
                    state = true;

            if (mouseButton == currentMouseState.RightButton)
                if (mouseButton == ButtonState.Pressed && prevMouseState.RightButton == ButtonState.Released)
                    state = true;

            return state;
        }

        public bool KeyPressed(params Keys[] keys)
        {
            foreach (Keys key in keys)
            {
                if (currentKeyState.IsKeyDown(key) && prevKeyState.IsKeyUp(key))
                    return true;
            }
            return false;
        }

        public bool KeyReleased(params Keys[] keys)
        {
            foreach (Keys key in keys)
            {
                if (currentKeyState.IsKeyUp(key) && prevKeyState.IsKeyDown(key))
                    return true;
            }
            return false;
        }

        public bool KeyDown(params Keys[] keys)
        {
            foreach (Keys key in keys)
            {
                if (currentKeyState.IsKeyDown(key))
                    return true;
            }
            return false;
        }
    }
}
