﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Discoverse.Systems
{
    public class fileManager
    {
        public void writeToFile(string folder, string file, List<string> data)
        {
            string loc = Assembly.GetExecutingAssembly().Location.Replace("\\Discoverse.exe", "");

            if (!Directory.Exists(loc + "\\" + folder))
                Directory.CreateDirectory(loc + "\\" + folder);

            if (File.Exists(loc + "\\" + folder + "/" + file))
            {
                using (var stream = File.Open(loc + "\\" + folder + "/" + file, FileMode.Truncate))
                {
                    using (StreamWriter writer = new StreamWriter(stream))
                    {
                        foreach (string line in data)
                        {
                            writer.WriteLine(line);
                        }
                    }
                }
            }
            else
            {
                using (var stream = File.Open(loc + "\\" + folder + "/" + file, FileMode.OpenOrCreate))
                {
                    using (StreamWriter writer = new StreamWriter(stream))
                    {
                        foreach (string line in data)
                        {
                            writer.WriteLine(line);
                        }
                    }
                }
            }
        }
    }
}
