﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discoverse.Systems
{
    public class emptyTile : Tile
    {
        public emptyTile()
        {
            this.material = GameScreen.Content.getTexture("prefab");
            this.type = "blank";
            this.diggingTime = 0f;
        }
    }

    public class dirtTile : Tile
    {
        public dirtTile()
        {
            this.material = GameScreen.Content.getTexture("prefab");
            this.type = "dirt";
            this.diggingTime = 0f;
        }
    }

    public class stoneTile : Tile
    {
        public stoneTile()
        {
            this.material = GameScreen.Content.getTexture("prefab");
            this.type = "stone";
            this.diggingTime = 0f;
        }
    }

    public class cobbleTile : Tile
    {
        public cobbleTile()
        {
            this.material = GameScreen.Content.getTexture("prefab");
            this.type = "cobble";
            this.diggingTime = 0f;
        }
    }

    public class abyssalTile : Tile
    {
        public abyssalTile()
        {
            this.material = GameScreen.Content.getTexture("prefab");
            this.type = "abyssal";
            this.diggingTime = 0f;
        }
    }

    // Elements
    public class bronzeTile : Tile
    {
        public bronzeTile()
        {
            this.material = GameScreen.Content.getTexture("prefab");
            this.itemClassType = "Discoverse.Elements.copperItem";
            this.type = "copper";
            this.diggingTime = 2f;
        }
    }

    public class coalTile : Tile
    {
        public coalTile()
        {
            this.material = GameScreen.Content.getTexture("prefab");
            this.itemClassType = "Discoverse.Elements.copperItem";
            this.type = "copper";
            this.diggingTime = 2f;
        }
    }

    public class ironTile : Tile
    {
        public ironTile()
        {
            this.material = GameScreen.Content.getTexture("prefab");
            this.itemClassType = "Discoverse.Elements.copperItem";
            this.type = "copper";
            this.diggingTime = 2f;
        }
    }

    public class silverTile : Tile
    {
        public silverTile()
        {
            this.material = GameScreen.Content.getTexture("prefab");
            this.itemClassType = "Discoverse.Elements.copperItem";
            this.type = "copper";
            this.diggingTime = 2f;
        }
    }

    public class goldTile : Tile
    {
        public goldTile()
        {
            this.material = GameScreen.Content.getTexture("prefab");
            this.itemClassType = "Discoverse.Elements.copperItem";
            this.type = "copper";
            this.diggingTime = 2f;
        }
    }

    public class diamondTile : Tile
    {
        public diamondTile()
        {
            this.material = GameScreen.Content.getTexture("prefab");
            this.itemClassType = "Discoverse.Elements.copperItem";
            this.type = "copper";
            this.diggingTime = 2f;
        }
    }
}
