﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discoverse.Systems
{
    public class BaseWorld
    {
        public Dictionary<string, float[][]> elements;

        List<string> elementsToCreate;

        int sizeX, sizeY;

        public BaseWorld(int mapSizeX, int mapSizeY)
        {
            sizeX = mapSizeX;
            sizeY = mapSizeY;

            elementsToCreate = new List<string>();
            elementsToCreate.Add("tiles");
            elementsToCreate.Add("materials");
            elementsToCreate.Add("diamonds");
            elementsToCreate.Add("gold");
            elementsToCreate.Add("silver");
            elementsToCreate.Add("bronze");
            elementsToCreate.Add("iron");
            elementsToCreate.Add("coal");

            elements = new Dictionary<string, float[][]>();
            foreach (string element in elementsToCreate)
            {
                float[][] baseNoise = Noise.GenerateWhiteNoise(sizeX, sizeY);
                //float[][] smoothNoise = Noise.GenerateSmoothNoise(baseNoise, 2);
                elements.Add(element, Noise.GeneratePerlinNoise(baseNoise, 10));
                /*if (element != "tiles")
                {
                    System.Windows.Forms.MessageBox.Show("Create world: " + element);
                    elements.Add(element, Noise.GeneratePerlinNoise(baseNoise, 10));
                } else
                {
                    elements.Add(element, Noise.GeneratePerlinNoise(baseNoise, 4));
                }*/
            }
        }
    }
}
