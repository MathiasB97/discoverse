﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Discoverse.Systems
{
    public class Tile
    {
        public Vector2 position;
        public Texture2D material;
        public string itemClassType;
        public string type;
        public bool collidable = true;
        public float lightIntensity = 1f;
        public float diggingTime;
        public float frame;

        public Rectangle BoundingBox
        {
            get
            {
                return new Rectangle((int)this.position.X, (int)this.position.Y, 16, 16);
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            int num = (int)(this.frame / this.diggingTime * 10f);
            spriteBatch.Draw(this.material, this.position, new Rectangle?(new Rectangle(num * 16, 0, 16, 16)), Color.White);
        }
    }
}
