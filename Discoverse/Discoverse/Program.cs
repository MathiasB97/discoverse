﻿using System;
using Steamworks;
using System.IO;
using System.Net;

namespace Discoverse
{
#if WINDOWS || LINUX
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            /*using (var game = new Game1())
                game.Run();*/
            if (SteamAPI.IsSteamRunning())
            {
                //Systems.Steam.init();
                using (var game = new Game1())
                {
                    game.Run();
                }
                //performDataUpload();
            } else
            {
                //System.Windows.Forms.MessageBox.Show("Steam is not running");
                using (var game = new Game1())
                {
                    game.Run();
                }
            }
            //if (Steamworks.SteamAPI.Init())
        }

        static void performDataUpload()
        {
            string sqlFile = Directory.GetCurrentDirectory() + "\\" + Discoverse.Systems.SQLiteManager.fileName;
            if (File.Exists(sqlFile))
            {
                System.Net.WebClient client = new System.Net.WebClient();
                try
                {
                    client.Credentials = new NetworkCredential("root", "Be03121997");
                    WebRequest sR = WebRequest.Create(@"http://redmountgames.xyz/gamedata/");
                    WebResponse sRes;
                    sRes = sR.GetResponse();
                    sRes.Close();

                    client.UploadFile(@"http://redmountgames.xyz/gamedata/", "PUT", @sqlFile);
                    client.Dispose();
                    client = null;
                }
                catch (Exception e)
                {
                    System.Windows.Forms.MessageBox.Show(e.Message);
                }
                /*client.Headers.Add("Content-Type", "application/octet-stream");
                try
                {
                    byte[] data = client.UploadFile("http://redmountgames.xyz/gamedata/upload.php", "POST", @sqlFile);
                    if (data != null)
                    {
                    }
                } catch (Exception web)
                {
                    System.Windows.Forms.MessageBox.Show(web.ToString());
                }*/
            }
        }
    }
#endif
}
