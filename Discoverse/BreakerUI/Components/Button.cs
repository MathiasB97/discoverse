﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Utilities;
using Microsoft.Xna.Framework.Input;

namespace BreakerUI.Components
{
    public class Button
    {
        Vector2 wposition;
        public Vector2 WPosition
        {
            get { return wposition; }
            set { this.wposition = value; }
        }

        Vector2 position;
        Vector2 size;

        Color bC;
        Color fC;

        string oldText;
        string text;

        public bool enabled = true;

        public string Text
        {
            get { return text; }
        }

        public Vector2 Position
        {
            get { return position; }
            set { this.position = value; }
        }

        public Vector2 Size
        {
            get { return size; }
        }

        bool pre_clicked;
        bool clicked;
        bool hover;
        string pic;

        public event EventHandler onButtonClick;
        SpriteFont font;
        Window parentWindow;

        public Window ParentWindow
        {
            get { return parentWindow; }
        }

        public Button(Vector2 pos, string text, Manager manager)
        {
            this.font = Manager.content.Load<SpriteFont>("Fonts/Express");

            this.position = pos;
            this.text = text;
            this.size = font.MeasureString(text);

            manager.addElement(this);
            //GameScreen.ui_holder.addElement(this);
        }

        public Button(Vector2 pos, string text, Window window, Manager manager)
        {
            this.font = Manager.content.Load<SpriteFont>("Fonts/Express");

            this.wposition = pos;
            this.text = text;
            this.size = font.MeasureString(text);
            manager.addElement(this);

            window.addParentElement(this);
        }

        public void setText(string t)
        {
            oldText = text;
            text = t;

            this.size = font.MeasureString(text);
        }

        public void Update()
        {
            Vector2 mousePos = Mouse.GetState().Position.ToVector2();

            if (enabled == false)
                return;

            if (hover == true)
                if (!pre_clicked)
                {
                    pic = "btn_";
                    bC = Color.Lerp(bC, Color.White, 0.1f);
                    fC = Color.Lerp(fC, Color.FromNonPremultiplied(7, 129, 255, 255), 0.25f);
                }
                else
                {
                    pic = "btn_";
                    bC = Color.Lerp(bC, Color.Black, 0.1f);
                    fC = Color.Lerp(fC, Color.FromNonPremultiplied(7, 129, 255, 255), 0.25f);
                }
            else
            {
                pic = "btn_";
                bC = Color.Lerp(bC, Color.Black, 0.1f);
                fC = Color.Lerp(fC, Color.White, 0.25f);
            }

            // Detecting when the mouse is over the button
            if (parentWindow != null)
            {
                if (!parentWindow.Focused)
                    hover = false;
            }

            if (parentWindow != null)
            {
                if (mousePos.X >= wposition.X && mousePos.Y >= wposition.Y)
                {
                    if (mousePos.X <= (wposition.X + (size.X)) && mousePos.Y <= (wposition.Y + 28))
                    {
                        hover = true;
                    }
                    else
                    {
                        hover = false;
                    }
                }
                else
                {
                    hover = false;
                }
            }
            else
            {
                if (mousePos.X >= (position.X - 16) && mousePos.Y >= position.Y)
                {
                    if (mousePos.X <= (position.X + (size.X + 16)) && mousePos.Y <= (position.Y + 28))
                    {
                        hover = true;
                    }
                    else
                    {
                        hover = false;
                    }
                }
                else
                {
                    hover = false;
                }
            }

            // Detecting when the mouse is 'clicking' on the button
            if (hover && Mouse.GetState().LeftButton == ButtonState.Pressed)
            {
                pre_clicked = true;
            }
            else
                if (clicked)
                pre_clicked = false;

            if (pre_clicked && Mouse.GetState().LeftButton == ButtonState.Released)
            {
                if (!clicked && hover)
                {
                    clicked = true;
                    onButtonClick(this, null);
                }
                else
                {
                    //clicked = false;
                    pre_clicked = false;
                }
            }
            else
            {
                clicked = false;
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (enabled != true)
                return;

            if (parentWindow != null)
            {
                spriteBatch.Draw(Manager.ui_texture, wposition - new Vector2(6, 0), UI_Textures.getUITexture(pic + "st"), Color.White);
                spriteBatch.Draw(Manager.ui_texture, new Rectangle((int)wposition.X, (int)wposition.Y, (int)size.X, 32), UI_Textures.getUITexture(pic + "in"), Color.White);
                spriteBatch.Draw(Manager.ui_texture, wposition + new Vector2(size.X, 0), UI_Textures.getUITexture(pic + "end"), Color.White);
            }
            else
            {
                spriteBatch.Draw(Manager.ui_texture, position - new Vector2(16, 0), UI_Textures.getUITexture(pic + "lt"), bC);
                spriteBatch.Draw(Manager.ui_texture, position + size - new Vector2(-1, size.Y), UI_Textures.getUITexture(pic + "rt"), bC);
                spriteBatch.Draw(Manager.ui_texture, position - new Vector2(16, -16), UI_Textures.getUITexture(pic + "lb"), bC);
                spriteBatch.Draw(Manager.ui_texture, position + size - new Vector2(-1, size.Y - 16), UI_Textures.getUITexture(pic + "rb"), bC);
                spriteBatch.Draw(Manager.ui_texture, new Rectangle((int)position.X, (int)position.Y, (int)size.X + 1, 16), UI_Textures.getUITexture(pic + "ti"), bC);
                spriteBatch.Draw(Manager.ui_texture, new Rectangle((int)position.X, (int)position.Y + 16, (int)size.X + 1, 16), UI_Textures.getUITexture(pic + "bi"), bC);
                /*spriteBatch.Draw(Manager.ui_texture, position - new Vector2(6, 0), UI_Textures.getUITexture(pic + "st"), Color.White);
                spriteBatch.Draw(Manager.ui_texture, new Rectangle((int)position.X, (int)position.Y, (int)size.X, 32), UI_Textures.getUITexture(pic + "in"), Color.White);
                spriteBatch.Draw(Manager.ui_texture, position + new Vector2(size.X, 0), UI_Textures.getUITexture(pic + "end"), Color.White);*/
            }

            if (text != string.Empty)
            {
                if (parentWindow != null)
                {
                    /*spriteBatch.DrawString(font, text, wposition + new Vector2(1, 9), Color.Gray);
                    spriteBatch.DrawString(font, text, wposition + new Vector2(0, 8), Color.White);*/
                }
                else
                {
                    /*spriteBatch.DrawString(font, text, position + new Vector2(1, 9), Color.Gray);
                    spriteBatch.DrawString(font, text, position + new Vector2(0, 8), Color.White);*/
                    spriteBatch.DrawString(font, text, position + new Vector2(0, 6), fC);
                }
            }
        }
    }
}
