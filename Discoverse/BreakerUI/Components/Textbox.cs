﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Utilities;
using Microsoft.Xna.Framework.Input;

namespace BreakerUI.Components
{
    public class Textbox
    {
        Vector2 position;
        Vector2 size;

        string oldText;
        string text = "";
        string placeholder = "";

        public bool enabled = true;

        char hideChar = ' ';
        Vector2 presize = Vector2.Zero;

        private KeyboardState kbState, lastKbState;
        private Keys[] keysToCheck = new Keys[] { Keys.A, Keys.B, Keys.C, Keys.D, Keys.E, Keys.F, Keys.G, Keys.H, Keys.I, Keys.J, Keys.K, Keys.L, Keys.M, Keys.N, Keys.O, Keys.P, Keys.Q, Keys.R, Keys.S, Keys.T, Keys.U, Keys.V, Keys.W, Keys.X, Keys.Y, Keys.Z, Keys.D0, Keys.D1, Keys.D2, Keys.D3, Keys.D4, Keys.D5, Keys.D6, Keys.D7, Keys.D8, Keys.D9, Keys.Space, Keys.Back, Keys.OemPeriod, Keys.Decimal, Keys.OemMinus, Keys.Divide, Keys.OemQuestion, Keys.OemSemicolon, Keys.OemPlus, Keys.OemComma, Keys.OemQuotes, Keys.OemTilde, Keys.LeftControl, Keys.RightControl, Keys.Delete };
        private int[] keyTime = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        bool shift = false;
        bool control = false;

        Color fC;
        Color bC;

        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }

        public Vector2 Size
        {
            get { return size; }
        }

        bool pre_clicked;
        bool focused;
        bool hover;

        string blinker = "|";

        public int maxLength;

        public event EventHandler onInputUpdate;
        public event EventHandler onTextboxClick;

        SpriteFont font;

        Texture2D prefab;

        public Textbox(Vector2 position, string s, Manager manager, char fieldChar = ' ', int charLimit = 120)
        {
            this.font = Manager.content.Load<SpriteFont>("Fonts/Express");

            this.position = position + new Vector2(16, 0);
            //this.text = text;
            this.placeholder = s;
            this.size = font.MeasureString(s);

            manager.addElement(this);

            if (fieldChar != ' ')
                hideChar = fieldChar;

            /* Initiate Keyboard States
            kbState = new KeyboardState();
            lastKbState = new KeyboardState();*/
            prefab = new Texture2D(Manager.Device, 1, 1);
            prefab.SetData<Color>(new Color[] { Color.White });

            maxLength = charLimit;
        }

        public void setText(string t, bool placeholder = false)
        {
            if (!placeholder)
            {
                oldText = text;
                text = t;
            }
            else
            {
                this.placeholder = t;
            }
            this.size = font.MeasureString(t);
        }

        public void Update()
        {
            Vector2 mousePos = Mouse.GetState().Position.ToVector2();

            if (enabled != true)
                return;

            if (focused)
            {
                size = font.MeasureString(text) + new Vector2(6, 0);
            }
            else
            {
                if (text.Length != 0)
                    size = font.MeasureString(text);
                else
                    size = font.MeasureString(placeholder);
                //size = new Vector2(6, 0);
            }

            // Detecting when the mouse is over the textbox
            if (mousePos.X >= position.X && mousePos.Y >= position.Y)
            {
                if (mousePos.X <= (position.X + (size.X)) && mousePos.Y <= (position.Y + 28))
                {
                    hover = true;
                    bC = Color.Lerp(bC, Color.White, 0.25f);
                    fC = Color.Lerp(fC, Color.FromNonPremultiplied(7, 129, 255, 255), 0.25f);
                }
                else
                {
                    hover = false;
                    bC = Color.Lerp(bC, Color.Black, 0.25f);
                    fC = Color.Lerp(fC, Color.White, 0.25f);
                }
            }
            else
            {
                hover = false;
                bC = Color.Lerp(bC, Color.Black, 0.25f);
                fC = Color.Lerp(fC, Color.White, 0.25f);
            }

            //Detecting when the mouse is 'clicking' on the button
            if (Mouse.GetState().LeftButton == ButtonState.Pressed)
            {
                if (hover)
                    pre_clicked = true;
                else
                    if (focused)
                    focused = false;
            }

            if (pre_clicked && Mouse.GetState().LeftButton == ButtonState.Released)
            {
                if (!focused && hover)
                {
                    focused = true;
                    onTextboxClick(this, null);
                }
                else
                {
                    pre_clicked = false;
                }
            }
            /*else
            {
                focused = false;
            }*/

            // Keyboard Input
            if (focused == true)
            {
                shift = false;
                control = false;
                kbState = Keyboard.GetState();
                if (kbState.IsKeyDown(Keys.Escape) || kbState.IsKeyDown(Keys.Enter)) // Handle Ending Input Focus
                    focused = false;
                if (kbState.IsKeyDown(Keys.LeftShift) || kbState.IsKeyDown(Keys.RightShift)) // Handle capitalization
                    shift = true;

                if (kbState.IsKeyDown(Keys.LeftControl) || kbState.IsKeyDown(Keys.RightControl))
                    control = true;

                int a = -1;
                foreach (Keys key in keysToCheck)
                {
                    a++;
                    if (lastKbState.IsKeyDown(key) && kbState.IsKeyDown(key))
                    {

                        keyTime[a]++; //for held key repeats and delays
                    }
                    else keyTime[a] = 0;
                    bool do_it = false;
                    if (keyTime[a] > 20)
                    {
                        do_it = true;
                        keyTime[a] = 7;
                    }

                    if (lastKbState.IsKeyUp(key) && kbState.IsKeyDown(key))
                        do_it = true;
                    if (do_it)
                    {
                        String newChar = "";
                        if (text.Length >= maxLength && key != Keys.Back) continue; //don't do anything if we reached size limit (unless is Back key)
                        //if (key != Keys.Back) continue;
                        switch (key)
                        {
                            case Keys.A:
                                newChar += "a";
                                break;
                            case Keys.B:
                                newChar += "b";
                                break;
                            case Keys.C:
                                newChar += "c";
                                break;
                            case Keys.D:
                                newChar += "d";
                                break;
                            case Keys.E:
                                newChar += "e";
                                break;
                            case Keys.F:
                                newChar += "f";
                                break;
                            case Keys.G:
                                newChar += "g";
                                break;
                            case Keys.H:
                                newChar += "h";
                                break;
                            case Keys.I:
                                newChar += "i";
                                break;
                            case Keys.J:
                                newChar += "j";
                                break;
                            case Keys.K:
                                newChar += "k";
                                break;
                            case Keys.L:
                                newChar += "l";
                                break;
                            case Keys.M:
                                newChar += "m";
                                break;
                            case Keys.N:
                                newChar += "n";
                                break;
                            case Keys.O:
                                newChar += "o";
                                break;
                            case Keys.P:
                                newChar += "p";
                                break;
                            case Keys.Q:
                                newChar += "q";
                                break;
                            case Keys.R:
                                newChar += "r";
                                break;
                            case Keys.S:
                                newChar += "s";
                                break;
                            case Keys.T:
                                newChar += "t";
                                break;
                            case Keys.U:
                                newChar += "u";
                                break;
                            case Keys.V:
                                newChar += "v";
                                break;
                            case Keys.W:
                                newChar += "w";
                                break;
                            case Keys.X:
                                newChar += "x";
                                break;
                            case Keys.Y:
                                newChar += "y";
                                break;
                            case Keys.Z:
                                newChar += "z";
                                break;
                            case Keys.D0:
                                if (!shift)
                                    newChar += "0";
                                else
                                    newChar += ")";
                                break;
                            case Keys.D1:
                                if (!shift)
                                    newChar += "1";
                                else
                                    newChar += "!";
                                break;
                            case Keys.D2:
                                if (!shift)
                                    newChar += "2";
                                else
                                    newChar += "@";
                                break;
                            case Keys.D3:
                                if (!shift)
                                    newChar += "3";
                                else
                                    newChar += "#";
                                break;
                            case Keys.D4:
                                if (!shift)
                                    newChar += "4";
                                else
                                    newChar += "$";
                                break;
                            case Keys.D5:
                                if (!shift)
                                    newChar += "5";
                                else
                                    newChar += "%";
                                break;
                            case Keys.D6:
                                if (!shift)
                                    newChar += "6";
                                else
                                    newChar += "^";
                                break;
                            case Keys.D7:
                                if (!shift)
                                    newChar += "7";
                                else
                                    newChar += "&";
                                break;
                            case Keys.D8:
                                if (!shift)
                                    newChar += "8";
                                else
                                    newChar += "*";
                                break;
                            case Keys.D9:
                                if (!shift)
                                    newChar += "9";
                                else
                                    newChar += "(";
                                break;
                            case Keys.Space:
                                newChar += " ";
                                break;
                            case Keys.OemPeriod:
                                if (!shift)
                                    newChar += ".";
                                else
                                    newChar += ">";
                                break;
                            case Keys.Decimal:
                                newChar += ".";
                                break;
                            case Keys.OemMinus:
                                if (!shift)
                                    newChar += "-";
                                else
                                    newChar += "_";
                                break;
                            case Keys.OemQuestion:
                                if (!shift)
                                    newChar += "/";
                                else
                                    newChar += "?";
                                break;
                            case Keys.OemComma:
                                if (!shift)
                                    newChar += ",";
                                else
                                    newChar += "<";
                                break;
                            case Keys.OemPlus:
                                if (!shift)
                                    newChar += "=";
                                else
                                    newChar += "+";
                                break;
                            case Keys.OemSemicolon:
                                if (!shift)
                                    newChar += ";";
                                else
                                    newChar += ":";
                                break;
                            case Keys.OemTilde:
                                if (!shift)
                                    newChar += "`";
                                else
                                    newChar += "`";
                                break;
                            case Keys.OemQuotes:
                                if (!shift)
                                    newChar += "'";
                                else
                                    newChar += '"';
                                break;
                            case Keys.Delete:
                                text = "";
                                break;
                            /*case Keys.Space: if (!((first_letter_restrictions) && (input_str.Length < 1))) newChar += " "; break;
                            case Keys.OemPeriod: if (!((first_letter_restrictions) && (input_str.Length < 1))) newChar += "."; break;
                            case Keys.Decimal: if (!((first_letter_restrictions) && (input_str.Length < 1))) newChar += "."; break;
                            case Keys.OemMinus: if (!((first_letter_restrictions) && (input_str.Length < 1))) { if (!shift) newChar += "-"; else newChar += "_"; } break;
                            case Keys.Divide: if (!((first_letter_restrictions) && (input_str.Length < 1))) newChar += "/"; break;
                            case Keys.OemQuestion: if (!((first_letter_restrictions) && (input_str.Length < 1))) { if (!shift) newChar += "/"; else newChar += "?"; } break;
                            case Keys.Back: if (input_str.Length != 0) input_str = input_str.Remove(input_str.Length - 1, 1); continue;*/
                            case Keys.Back:
                                if (text.Length != 0)
                                    text = text.Remove(text.Length - 1, 1);
                                continue;
                        }
                        if (shift)
                            newChar = newChar.ToUpper();
                        if (control)
                        {
                            string trimText = "";
                            string ctext = System.Windows.Forms.Clipboard.GetText();
                            if ((text.Length + ctext.Length) > maxLength)
                            {
                                trimText = ctext.Remove(maxLength - text.Length);
                            }
                            else
                            {
                                trimText = ctext;
                            }
                            newChar = trimText;
                        }
                        /*if (control)
                            text = System.Windows.Forms.Clipboard.GetText();*/
                        //input_str += newChar;
                        text += newChar;
                        onInputUpdate(this, null);
                        break;
                    }
                }
                lastKbState = kbState;
            }
        }

        void Textbox_onInputUpdate(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (enabled != true)
                return;

            spriteBatch.Draw(Manager.ui_texture, position - new Vector2(5, 1), UI_Textures.getUITexture("tbx_lt"), bC);
            spriteBatch.Draw(Manager.ui_texture, position - new Vector2(5, -15), UI_Textures.getUITexture("tbx_lb"), bC);
            spriteBatch.Draw(Manager.ui_texture, position + size - new Vector2(6, size.Y + 1), UI_Textures.getUITexture("tbx_rt"), bC);
            spriteBatch.Draw(Manager.ui_texture, position + size - new Vector2(6, size.Y - 15), UI_Textures.getUITexture("tbx_rb"), bC);

            spriteBatch.Draw(Manager.ui_texture, new Rectangle((int)position.X + 11, (int)position.Y - 1, (int)size.X - 17, 16), UI_Textures.getUITexture("tbx_ti"), bC);
            spriteBatch.Draw(Manager.ui_texture, new Rectangle((int)position.X + 11, (int)position.Y - 1 + 16, (int)size.X - 17, 16), UI_Textures.getUITexture("tbx_bi"), bC);

            /*spriteBatch.Draw(Manager.ui_texture, position - new Vector2(6, 0), UI_Textures.getUITexture("editbox_st"), Color.White);
            spriteBatch.Draw(Manager.ui_texture, new Rectangle((int)position.X, (int)position.Y, (int)size.X, 32), UI_Textures.getUITexture("editbox_in"), Color.White);
            spriteBatch.Draw(Manager.ui_texture, position + new Vector2(size.X, 0), UI_Textures.getUITexture("editbox_end"), Color.White);*/

            if (text != string.Empty)
            {
                if (hideChar == ' ')
                    spriteBatch.DrawString(font, text, position + new Vector2(0, 4), fC);
                else
                {
                    string t = text;
                    char[] s = t.ToCharArray();
                    for (int i = 0; i < t.Length; i++)
                    {
                        s[i] = hideChar;
                    }
                    /*foreach (char letter in t.ToCharArray())
                    {
                        letter = hideChar;
                    }*/
                    spriteBatch.DrawString(font, new string(s), position + new Vector2(0, 4), fC);
                }
            }
            else
            {
                if (!focused)
                    spriteBatch.DrawString(font, placeholder, position + new Vector2(0, 4), fC);
            }

            // Blinker
            if (focused)
            {
                spriteBatch.DrawString(font, blinker, position + new Vector2(font.MeasureString(text).X - 2, 4), fC);
                //spriteBatch.DrawString(font, (maxLength - text.Length).ToString(), position + new Vector2(-35, 8), Color.SlateGray);
            }
        }
    }
}
