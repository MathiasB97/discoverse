﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Utilities;
using Microsoft.Xna.Framework.Input;

namespace BreakerUI.Components
{
    public class Label
    {
        Vector2 position;
        Vector2 size;

        string text;

        public string Text
        {
            get { return text; }
        }

        public bool enabled;

        SpriteFont font;
        Color labelColor;

        public Label(Vector2 position, string text, Manager manager, Color? labelColor = null)
        {
            this.font = Manager.content.Load<SpriteFont>("Fonts/Express");

            this.position = position + new Vector2(16, 0);
            this.text = text;
            this.size = font.MeasureString(text);

            this.labelColor = labelColor ?? Color.White;

            manager.addElement(this);
        }

        public void setText(string t)
        {
            text = t;
        }

        public void Update()
        {
            if (enabled != true)
                return;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (enabled != true)
                return;

            if (text != string.Empty)
            {
                //spriteBatch.DrawString(font, text, position + new Vector2(1, 9), Color.Gray);
                spriteBatch.DrawString(font, text, position + new Vector2(0, 8), labelColor);
            }
        }
    }
}
