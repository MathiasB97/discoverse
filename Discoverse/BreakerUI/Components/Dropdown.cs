﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Utilities;
using Microsoft.Xna.Framework.Input;

namespace BreakerUI.Components
{
    public class Dropdown
    {
        Vector2 position;
        Vector2 size;

        string oldText;
        string text;

        List<string> items;
        List<string> itemIPO;
        List<Color> itemC;

        public bool enabled = true;

        public string Text
        {
            get { return text; }
        }

        public Vector2 Position
        {
            get { return position; }
            set { this.position = value; }
        }

        public Vector2 Size
        {
            get { return size; }
        }

        bool pre_clicked;
        bool clicked;
        bool hover;


        bool showing;

        bool item_hover;
        bool item_clicked;

        string dro;
        string pic;

        public event EventHandler onButtonClick;

        public event EventHandler onDropdownItemClick;

        SpriteFont font;

        Color fC;
        Color bC;

        //Color iC;

        public Dropdown(Vector2 position, string text, Manager manager)
        {
            this.font = Manager.content.Load<SpriteFont>("Fonts/Express");

            items = new List<string>();
            itemIPO = new List<string>();
            itemC = new List<Color>();

            this.position = position + new Vector2(16, 0);
            this.text = text;
            this.size = font.MeasureString(text);

            manager.addElement(this);
        }

        public void addItemToList(string item)
        {
            items.Add(item);
            itemIPO.Add("btn_");
            itemC.Add(Color.FromNonPremultiplied(7, 129, 255, 255));
        }

        public void setText(string t)
        {
            oldText = text;
            text = t;
        }

        public void Update()
        {
            Vector2 mousePos = Mouse.GetState().Position.ToVector2();

            if (enabled != true)
                return;

            if (hover == true)
                if (!pre_clicked)
                {
                    dro = "drop_hover_";
                    //pic = "btn_hover_";
                    bC = Color.Lerp(bC, Color.White, 0.1f);
                    fC = Color.Lerp(fC, Color.FromNonPremultiplied(7, 129, 255, 255), 0.1f);
                }
                else
                {
                    dro = "drop_clicked_";
                    //pic = "btn_clicked_";
                    bC = Color.Lerp(bC, Color.Black, 0.1f);
                    fC = Color.Lerp(fC, Color.FromNonPremultiplied(7, 129, 255, 255), 0.1f);
                }
            else
            {
                dro = "drop_";
                pic = "btn_";
                bC = Color.Lerp(bC, Color.Black, 0.1f);
                fC = Color.Lerp(fC, Color.White, 0.1f);
            }

            // Detecting when the mouse is 'pre-clicking' on the button
            if (hover && Mouse.GetState().LeftButton == ButtonState.Pressed)
            {
                pre_clicked = true;
            }
            else
            {
                if (clicked)
                    pre_clicked = false;
            }

            // Detect when clicking on a button
            if (pre_clicked && Mouse.GetState().LeftButton == ButtonState.Released)
            {
                if (hover)
                {
                    clicked = true;
                    onButtonClick(this, null);
                }
                else
                    pre_clicked = false;
            }
            else
            {
                clicked = false;
            }

            if (clicked)
            {
                if (showing)
                    showing = false;
                else
                    showing = true;
            }

            // Detecting when the mouse is over the drop button
            if (mousePos.X >= (position.X + size.X + 4) && mousePos.Y >= position.Y)
            {
                if (mousePos.X <= (position.X + (size.X + 32)) && mousePos.Y <= (position.Y + 28))
                    hover = true;
                else
                    hover = false;
            }
            else
            {
                hover = false;
            }

            // Detect when the mouse is over an item
            for (int i = 0; i < items.Count; i++)
            {
                float x = 20 + position.X - 6;
                float y = position.Y + (31 + (i * 32));
                float sX = font.MeasureString(items[i]).X;
                float sY = 32;

                if (showing)
                {
                    if (mousePos.X >= x && mousePos.Y >= y)
                    {
                        if (mousePos.X <= (x + sX) && mousePos.Y <= (y + sY))
                        {
                            item_hover = true;
                            itemIPO[i] = "btn_hover_";
                            //iC = Color.Lerp(fC, Color.Red, 0.25f);
                            itemC[i] = Color.Lerp(itemC[i], Color.Black, 0.1f);
                        }
                        else
                        {
                            itemIPO[i] = "btn_";
                            item_hover = false;
                            itemC[i] = Color.Lerp(itemC[i], Color.FromNonPremultiplied(7, 129, 255, 255), 0.1f);
                            //iC = Color.Lerp(fC, Color.FromNonPremultiplied(7, 129, 255, 255), 0.25f);
                        }
                    }
                    else
                    {
                        itemIPO[i] = "btn_";
                        item_hover = false;
                        itemC[i] = Color.Lerp(itemC[i], Color.FromNonPremultiplied(7, 129, 255, 255), 0.1f);
                    }
                }
            }

            // Detect when the mouse is clicking on an item
            if (showing && Mouse.GetState().LeftButton == ButtonState.Pressed)
            {
                for (int i = 0; i < items.Count; i++)
                {
                    if (itemIPO[i] == "btn_hover_")
                    {
                        item_clicked = true;
                        oldText = text;
                        text = items[i];

                        showing = false;

                        if (oldText != text)
                            onDropdownItemClick(this, null);
                    }
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (enabled != true)
                return;

            spriteBatch.Draw(Manager.ui_texture, position - new Vector2(16, 0), UI_Textures.getUITexture(pic + "lt"), bC);
            spriteBatch.Draw(Manager.ui_texture, position - new Vector2(16, -16), UI_Textures.getUITexture(pic + "lb"), bC);
            spriteBatch.Draw(Manager.ui_texture, position + size - new Vector2(-17, size.Y), UI_Textures.getUITexture(pic + "rt"), bC);
            spriteBatch.Draw(Manager.ui_texture, position + size - new Vector2(-17, size.Y - 16), UI_Textures.getUITexture(pic + "rb"), bC);
            spriteBatch.Draw(Manager.ui_texture, new Rectangle((int)position.X, (int)position.Y, (int)size.X + 17, 16), UI_Textures.getUITexture(pic + "ti"), bC);
            spriteBatch.Draw(Manager.ui_texture, new Rectangle((int)position.X, (int)position.Y + 16, (int)size.X + 17, 16), UI_Textures.getUITexture(pic + "bi"), bC);

            //spriteBatch.Draw(ScreenManager.Instance.textureManager.getTextureFromName("prefab"), new Rectangle((int)position.X + (int)size.X + 2, (int)position.Y, 1, 32), fC * 0.25f);

            spriteBatch.Draw(Manager.ui_texture, position + new Vector2(size.X - 1, 0), UI_Textures.getUITexture("drp"), fC);
            if (showing)
                for (int i = 0; i < items.Count; i++)
                {
                    spriteBatch.Draw(Manager.ui_texture, position - new Vector2(16, -32 + (i * -32)), UI_Textures.getUITexture(pic + "lt"), Color.White);
                    spriteBatch.Draw(Manager.ui_texture, position - new Vector2(16, -48 + (i * -32)), UI_Textures.getUITexture(pic + "lb"), Color.White);
                    spriteBatch.Draw(Manager.ui_texture, position + size - new Vector2(-17, (size.Y - 32) + (i * -32)), UI_Textures.getUITexture(pic + "rt"), Color.White);
                    spriteBatch.Draw(Manager.ui_texture, position + size - new Vector2(-17, (size.Y - 48) + (i * -32)), UI_Textures.getUITexture(pic + "rb"), Color.White);
                    spriteBatch.Draw(Manager.ui_texture, new Rectangle((int)position.X, (int)position.Y + 32 + (i * 32), (int)size.X + 17, 16), UI_Textures.getUITexture(pic + "ti"), Color.White);
                    spriteBatch.Draw(Manager.ui_texture, new Rectangle((int)position.X, (int)position.Y + 48 + (i * 32), (int)size.X + 17, 16), UI_Textures.getUITexture(pic + "bi"), Color.White);

                    /*spriteBatch.Draw(Manager.ui_texture, new Rectangle(20 + (int)position.X - 6, (int)position.Y + (31 + (i * 32)), 6, 32), UI_Textures.getUITexture(itemIPO[i] + "st"), Color.White);
                    spriteBatch.Draw(Manager.ui_texture, new Rectangle(20 + (int)position.X, (int)position.Y + (31 + (i * 32)), (int)font.MeasureString(items[i]).X, 32), UI_Textures.getUITexture(itemIPO[i] + "in"), Color.White);
                    spriteBatch.Draw(Manager.ui_texture, new Rectangle(20 + (int)position.X + (int)font.MeasureString(items[i]).X, (int)position.Y + (31 + (i * 32)), 6, 32), UI_Textures.getUITexture(itemIPO[i] + "end"), Color.White);*/
                    spriteBatch.DrawString(font, items[i], position + new Vector2(0, 38 + (i * 32)), itemC[i]);
                }

            if (text != string.Empty)
            {
                /*spriteBatch.DrawString(font, text, position + new Vector2(1, 9), Color.Gray);
                spriteBatch.DrawString(font, text, position + new Vector2(0, 8), Color.White);*/
                spriteBatch.DrawString(font, text, position + new Vector2(-8, 6), fC);
            }
        }
    }
}
