﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Utilities;
using Microsoft.Xna.Framework.Input;

namespace BreakerUI.Components
{
    public class Window
    {
        Vector2 position;
        Vector2 size;

        bool focus;

        public bool Focused
        {
            get { return focus; }
        }

        public Vector2 Position
        {
            get { return position; }
            set { this.position = value; }
        }

        string text;

        public string Text
        {
            get { return text; }
        }

        bool hover;

        SpriteFont font;

        Color windowColor;
        Color activeColor;
        Color focusColor = Color.Gray;

        List<Button> parentButtons;

        public Window(Vector2 position, Vector2 size, string text, Color windowColor, Manager manager)
        {
            this.font = Manager.content.Load<SpriteFont>("Fonts/Express");

            this.position = position + new Vector2(16, 0);
            this.text = text;
            this.size = size;
            this.activeColor = windowColor;

            manager.addElement(this);

            parentButtons = new List<Button>();
        }

        public void addParentElement(Button elem)
        {
            parentButtons.Add(elem);
        }

        public void setText(string t)
        {
            text = t;
        }

        public void Update()
        {
            // Handle window parent
            foreach (Button button in parentButtons)
            {
                if (button.ParentWindow != null)
                    button.WPosition = position + new Vector2(16, 16);
            }

            // Handle window movement

            Vector2 mousePos = Mouse.GetState().Position.ToVector2();
            // Detecting when the mouse is over the button
            if (mousePos.X >= position.X && mousePos.Y >= position.Y)
            {
                if (mousePos.X <= (position.X + (size.X) + 8) && mousePos.Y <= (position.Y + (size.Y - 8)))
                {
                    hover = true;
                }
                else
                {
                    hover = false;
                }
            }
            else
            {
                hover = false;
            }

            if (focus && Mouse.GetState().RightButton == ButtonState.Pressed)
            {
                //position = new Vector2(mousePos.X - (size.X / 2), mousePos.Y - (size.Y / 16));

                position = new Vector2(mousePos.X, mousePos.Y);
            }

            if (Mouse.GetState().LeftButton == ButtonState.Pressed)
            {
                if (hover)
                    focus = true;
                else
                    focus = false;
            }

            if (focus)
            {
                if (windowColor != activeColor)
                    windowColor = activeColor;
            }
            else
            {
                if (windowColor != focusColor)
                    windowColor = focusColor;
            }

        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (text != string.Empty)
            {
                spriteBatch.Draw(Manager.ui_texture, position, UI_Textures.getUITexture("window_lt"), Color.Lerp(windowColor, Color.White, 0.25f)); // Left Top
                spriteBatch.Draw(Manager.ui_texture, position + new Vector2(size.X, 0), UI_Textures.getUITexture("window_rt"), Color.Lerp(windowColor, Color.White, 0.25f)); // Right Top
                spriteBatch.Draw(Manager.ui_texture, new Rectangle((int)position.X + 8, (int)position.Y, (int)size.X - 8, 8), UI_Textures.getUITexture("window_tl"), Color.Lerp(windowColor, Color.White, 0.25f)); // Title Area
                // Sides
                int offset = 16;
                spriteBatch.Draw(Manager.ui_texture, new Rectangle((int)position.X, (int)position.Y + offset, 8, (int)size.Y - offset), UI_Textures.getUITexture("window_sl"), Color.Lerp(windowColor, Color.White, 0.25f)); // Left Side
                spriteBatch.Draw(Manager.ui_texture, new Rectangle((int)position.X + (int)size.X, (int)position.Y + offset, 8, (int)size.Y - offset), UI_Textures.getUITexture("window_sr"), Color.Lerp(windowColor, Color.White, 0.25f)); // Right Side
                // Color Title Part Start
                spriteBatch.Draw(Manager.ui_texture, new Rectangle((int)position.X, (int)position.Y + 8, 8, 8), UI_Textures.getUITexture("window_sl"), Color.Lerp(windowColor, Color.White, 0.25f)); // Left Side
                spriteBatch.Draw(Manager.ui_texture, new Rectangle((int)position.X + (int)size.X, (int)position.Y + 8, 8, 8), UI_Textures.getUITexture("window_sr"), Color.Lerp(windowColor, Color.White, 0.25f)); // Right Side
                spriteBatch.Draw(Manager.ui_texture, new Rectangle((int)position.X + 8, (int)position.Y + 8, (int)size.X - 8, 8), UI_Textures.getUITexture("window_inside"), Color.Lerp(windowColor, Color.White, 0.25f));

                // Color Title Part End

                spriteBatch.Draw(Manager.ui_texture, position + new Vector2(0, size.Y), UI_Textures.getUITexture("window_lb"), Color.Lerp(windowColor, Color.White, 0.25f)); // Left Bottom
                spriteBatch.Draw(Manager.ui_texture, position + size, UI_Textures.getUITexture("window_rb"), Color.Lerp(windowColor, Color.White, 0.25f)); // Right Bottom
                spriteBatch.Draw(Manager.ui_texture, new Rectangle((int)position.X + 8, (int)position.Y + (int)size.Y, (int)size.X - 8, 8), UI_Textures.getUITexture("window_bl"), Color.Lerp(windowColor, Color.White, 0.25f)); // Bottom Line

                // Inside window
                spriteBatch.Draw(Manager.ui_texture, new Rectangle((int)position.X + 8, (int)position.Y + offset, (int)size.X - 8, (int)size.Y - offset), UI_Textures.getUITexture("window_inside"), Color.Lerp(windowColor, Color.White, 0.25f)); // Title Area

                // Title
                spriteBatch.DrawString(font, text, position + new Vector2((size.X / 2) - (font.MeasureString(text).X / 2), 2), Color.Black);
            }
        }
    }
}
