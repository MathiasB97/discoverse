﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace BreakerUI
{
    public class UI_Textures
    {
        public static int uiCount;
        public static List<Rectangle> uiLocations;
        public static List<string> uiNames;
        public UI_Textures()
        {
            uiNames = new List<string>();
            uiLocations = new List<Rectangle>();

            // Button
            addTextureLocation("btn_lt", new Rectangle(0, 0, 16, 16));
            addTextureLocation("btn_rt", new Rectangle(112, 0, 16, 16));
            addTextureLocation("btn_lb", new Rectangle(0, 48, 16, 16));
            addTextureLocation("btn_rb", new Rectangle(112, 48, 16, 16));
            addTextureLocation("btn_ti", new Rectangle(16, 0, 16, 16));
            addTextureLocation("btn_bi", new Rectangle(16, 48, 16, 16));

            // Dropdown
            addTextureLocation("drp", new Rectangle(480, 0, 32, 32));

            // Textbox
            addTextureLocation("tbx_lt", new Rectangle(128, 0, 16, 16));
            addTextureLocation("tbx_rt", new Rectangle(128 + 48, 0, 16, 16));
            addTextureLocation("tbx_lb", new Rectangle(128, 48, 16, 16));
            addTextureLocation("tbx_rb", new Rectangle(128 + 48, 48, 16, 16));
            addTextureLocation("tbx_ti", new Rectangle(128 + 16, 0, 16, 16));
            addTextureLocation("tbx_bi", new Rectangle(128 + 16, 48, 16, 16));

            // Window
            addTextureLocation("window_lt", new Rectangle(128, 0, 8, 8));
            addTextureLocation("window_rt", new Rectangle(184, 0, 8, 8));
            addTextureLocation("window_lb", new Rectangle(128, 56, 8, 8));
            addTextureLocation("window_rb", new Rectangle(184, 56, 8, 8));

            addTextureLocation("window_sl", new Rectangle(128, 8, 8, 8));
            addTextureLocation("window_sr", new Rectangle(184, 8, 8, 8));

            addTextureLocation("window_tl", new Rectangle(128 + 8, 0, 8, 8));
            addTextureLocation("window_inside", new Rectangle(128 + 8, 8, 8, 8));
            addTextureLocation("window_bl", new Rectangle(128 + 8, 56, 8, 8));

            /* Obselete UI Data
            // Regular Button
            addTextureLocation("btn_st", new Rectangle(0, 0, 6, 32));
            addTextureLocation("btn_in", new Rectangle(32, 0, 2, 32));
            addTextureLocation("btn_end", new Rectangle(58, 0, 6, 32));
            // Clicked Button
            addTextureLocation("btn_clicked_st", new Rectangle(0, 32, 6, 32));
            addTextureLocation("btn_clicked_in", new Rectangle(32, 32, 2, 32));
            addTextureLocation("btn_clicked_end", new Rectangle(58, 32, 6, 32));
            // Hovered Button
            addTextureLocation("btn_hover_st", new Rectangle(0, 64, 6, 32));
            addTextureLocation("btn_hover_in", new Rectangle(32, 64, 2, 32));
            addTextureLocation("btn_hover_end", new Rectangle(58, 64, 6, 32));
            // Dropdown Button
            addTextureLocation("drop_btn", new Rectangle(64, 0, 32, 32));
            addTextureLocation("drop_hover_btn", new Rectangle(64, 64, 32, 32));
            addTextureLocation("drop_clicked_btn", new Rectangle(64, 32, 32, 32));
            // Editbox
            addTextureLocation("editbox_st", new Rectangle(96, 0, 6, 32));
            addTextureLocation("editbox_in", new Rectangle(102, 0, 2, 32));
            addTextureLocation("editbox_end", new Rectangle(96 + 32 - 6, 0, 6, 32));
            // Window
            addTextureLocation("window_lt", new Rectangle(96, 0, 8, 8));
            addTextureLocation("window_rt", new Rectangle(120, 0, 8, 8));
            addTextureLocation("window_lb", new Rectangle(96, 24, 8, 8));
            addTextureLocation("window_rb", new Rectangle(120, 24, 8, 8));

            addTextureLocation("window_sl", new Rectangle(96, 8, 8, 16));
            addTextureLocation("window_sr", new Rectangle(120, 8, 8, 16));

            addTextureLocation("window_tl", new Rectangle(104, 0, 16, 8));
            addTextureLocation("window_inside", new Rectangle(104, 16, 8, 8));
            addTextureLocation("window_bl", new Rectangle(104, 24, 16, 8));
            */
        }
        public static void addTextureLocation(string uiName, Rectangle location)
        {
            uiNames.Add(uiName);
            uiLocations.Add(location);

            uiCount = uiLocations.Count;
        }

        public static Rectangle getUITexture(string uiElementName)
        {
            Rectangle rect = Rectangle.Empty;

            for (int i = 0; i < uiCount; i++)
            {
                if (uiNames[i] == uiElementName)
                {
                    rect = uiLocations[i];
                }
            }

            return rect;
        }
    }
}
