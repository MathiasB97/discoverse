﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

using BreakerUI.Components;

namespace BreakerUI
{
    public class Manager
    {
        private List<Button> buttons;
        private List<Dropdown> dropdowns;
        private List<Label> labels;
        private List<Textbox> textboxes;
        private List<Window> windows;

        private static GraphicsDevice device;

        public static GraphicsDevice Device
        {
            get { return device; }
        }

        public UI_Textures ui;
        public static ContentManager content;

        public static Texture2D ui_texture;

        public Manager(ContentManager m_content, Texture2D gui, GraphicsDevice gDevice)
        {
            ui = new UI_Textures();
            //this.content = content;
            content = m_content;

            device = gDevice;
            

            ui_texture = gui;

            buttons = new List<Button>();
            dropdowns = new List<Dropdown>();
            labels = new List<Label>();
            textboxes = new List<Textbox>();

            windows = new List<Window>();
        }

        public void Dispose(Button elem)
        {
            for (int i = 0; i < buttons.Count; i++)
            {
                if (buttons[i] == elem)
                    buttons.RemoveAt(i);
            }
        }

        public void Dispose(Dropdown elem)
        {
            for (int i = 0; i < dropdowns.Count; i++)
            {
                if (dropdowns[i] == elem)
                    dropdowns.RemoveAt(i);
            }
        }

        public void Dispose(Label elem)
        {
            for (int i = 0; i < labels.Count; i++)
            {
                if (labels[i] == elem)
                    labels.RemoveAt(i);
            }
        }

        public void Dispose(Textbox elem)
        {
            for (int i = 0; i < textboxes.Count; i++)
            {
                if (textboxes[i] == elem)
                    textboxes.RemoveAt(i);
            }
        }

        public void Dispose(Window elem)
        {
            for (int i = 0; i < windows.Count; i++)
            {
                if (windows[i] == elem)
                    windows.RemoveAt(i);
            }
        }

        public void Update()
        {
            foreach (Button btn in buttons)
                btn.Update();

            foreach (Dropdown drop in dropdowns)
                drop.Update();

            foreach (Label label in labels)
                label.Update();

            foreach (Textbox textbox in textboxes)
                textbox.Update();

            foreach (Window window in windows)
                window.Update();
        }

        public void UnloadContent()
        {
            buttons.Clear();
            dropdowns.Clear();
            labels.Clear();
            windows.Clear();
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (Window window in windows)
                window.Draw(spriteBatch);

            foreach (Button btn in buttons)
                btn.Draw(spriteBatch);
            foreach (Label label in labels)
                label.Draw(spriteBatch);
            foreach (Textbox textbox in textboxes)
                textbox.Draw(spriteBatch);
            foreach (Dropdown drop in dropdowns)
                drop.Draw(spriteBatch);
        }

        public void addElement(Button elem)
        {
            if (elem != null)
            {
                buttons.Add(elem);

                elem.onButtonClick += elem_onButtonClick;
            }
        }

        public void addElement(Dropdown elem)
        {
            if (elem != null)
            {
                dropdowns.Add(elem);

                elem.onButtonClick += elem_onButtonClick;
                elem.onDropdownItemClick += elem_onDropdownItemClick;
            }
        }

        public void addElement(Label elem)
        {
            if (elem != null)
            {
                labels.Add(elem);
            }
        }

        public void addElement(Textbox elem)
        {
            if (elem != null)
            {
                textboxes.Add(elem);
                elem.onTextboxClick += elem_onTextboxClick;
                elem.onInputUpdate += elem_onInputUpdate;
            }
        }

        public void addElement(Window elem)
        {
            if (elem != null)
            {
                windows.Add(elem);
            }
        }

        void elem_onTextboxClick(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }

        void elem_onInputUpdate(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }

        void elem_onDropdownItemClick(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }

        void elem_onButtonClick(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }
    }
}
