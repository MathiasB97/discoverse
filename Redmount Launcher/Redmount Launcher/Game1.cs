﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Redmount_Launcher.Systems;
using System.Collections.Generic;

namespace Redmount_Launcher
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Texture2D prefab;

        Texture2D logo;
        Color backgroundColor = Color.White;
        SpriteFont fontBig;
        SpriteFont fontSmall;

        public static Game1 game;

        NotificationManager notifications;

        BreakerUI.Manager UI;

        List<NotificationContent> c;

        BreakerUI.Components.Textbox username;
        BreakerUI.Components.Textbox password;
        BreakerUI.Components.Button loginBtn;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            game = this;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            Window.IsBorderless = true;
            this.IsMouseVisible = true;

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        /// 

        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            UI = new BreakerUI.Manager(Content, Content.Load<Texture2D>("GUI"), GraphicsDevice);

            notifications = new NotificationManager(Content);

            // TODO: use this.Content to load your game content here
            prefab = new Texture2D(GraphicsDevice, 1, 1);
            prefab.SetData<Color>(new Color[] { Color.White });

            logo = Content.Load<Texture2D>("redmount");
            fontSmall = Content.Load<SpriteFont>("Fonts/Lekton");
            fontBig = Content.Load<SpriteFont>("Fonts/Express2X");

            username = new BreakerUI.Components.Textbox(Vector2.Zero, "Username", UI);
            username.Position = new Vector2(GraphicsDevice.Viewport.Width / 2 - username.Size.X / 2, 200);
            username.onInputUpdate += Username_onInputUpdate;

            password = new BreakerUI.Components.Textbox(Vector2.Zero, "Password", UI, '*');
            password.Position = new Vector2(GraphicsDevice.Viewport.Width / 2 - password.Size.X / 2, 250);
            password.onInputUpdate += Password_onInputUpdate;

            loginBtn = new BreakerUI.Components.Button(Vector2.Zero, "Log in", UI);
            loginBtn.Position = new Vector2(GraphicsDevice.Viewport.Width / 2 - loginBtn.Size.X / 2, GraphicsDevice.Viewport.Height - 150);
            loginBtn.onButtonClick += LoginBtn_onButtonClick;

            //notifications.addNotification("This is a test", 0, "test");
            c = new List<NotificationContent>();
            c.Add(new NotificationContent("register"));
            c.Add(new NotificationContent("on"));
            c.Add(new NotificationContent("~XB1.X~"));

            for (int i = 0; i < c.Count; i++)
            {

            }
        }

        private void LoginBtn_onButtonClick(object sender, System.EventArgs e)
        {
        }

        private void Password_onInputUpdate(object sender, System.EventArgs e)
        {
            BreakerUI.Components.Textbox textbox = sender as BreakerUI.Components.Textbox;
            textbox.Position = new Vector2(GraphicsDevice.Viewport.Width / 2 - textbox.Size.X / 2, 250);
        }

        private void Username_onInputUpdate(object sender, System.EventArgs e)
        {
            //throw new System.NotImplementedException();
            BreakerUI.Components.Textbox textbox = sender as BreakerUI.Components.Textbox;
            textbox.Position = new Vector2(GraphicsDevice.Viewport.Width / 2 - textbox.Size.X / 2, 200);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            UI.Update();
            notifications.Update(gameTime);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(backgroundColor);

            spriteBatch.Begin();

            spriteBatch.Draw(prefab, new Rectangle(0, 0, GraphicsDevice.Viewport.Width, 80), Color.Black);
            spriteBatch.Draw(logo, new Rectangle(GraphicsDevice.Viewport.Width / 2 - (logo.Width / 4), 10, logo.Width / 2, logo.Height / 2), Color.White);
            //spriteBatch.Draw(prefab, new Rectangle(0, 80, GraphicsDevice.Viewport.Width, 4), Color.FromNonPremultiplied(197, 175, 85, 255));
            spriteBatch.Draw(prefab, new Rectangle(0, 80, GraphicsDevice.Viewport.Width, 4), Color.FromNonPremultiplied(49, 138, 186, 255));
            spriteBatch.DrawString(fontBig, "Log in", new Vector2(GraphicsDevice.Viewport.Width / 2 - fontBig.MeasureString("Log In").X / 2, 90), Color.Black);

            int i = 0;
            foreach (NotificationContent content in c)
            {
                i += 1;
            }
            /*for (int i = 0; i < c.Count; i++)
            {
                spriteBatch.DrawString(fontSmall, c[i].Text, new Vector2(25 + (i * 100), 25), Color.White);
                spriteBatch.Draw(prefab, new Rectangle(25 + (i + 100), 25 + 8, 10, 2), Color.Red);
                /*float lenghS = fontSmall.MeasureString(" ").X;
                int wordLen = c[i].Text.Length;
                if (i > 0)
                    spriteBatch.DrawString(fontSmall, c[i].Text, new Vector2(25 + lenghS + (wordLen * 12), 25), Color.White);
                else
                    spriteBatch.DrawString(fontSmall, c[i].Text, new Vector2(25, 25), Color.White);
            }*/

            UI.Draw(spriteBatch);
            notifications.Draw(spriteBatch);

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
